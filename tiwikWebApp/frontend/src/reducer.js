import {
  APP_LOAD,
  SIGNUP_PAGE_LOADED,
  GOOGLE_SIGNUP_SUCCESS,
  SIGNUP_SUCCESS,
  LOGOUT_SUCCESS,
  ARTICLE_SUBMITTED,
  LOGIN_SUCCESS,
  GOOGLE_LOGIN_SUCCESS,
  PROFILE_UPDATED,
  PROFILE_FOLLOWED,
  PROFILE_UNFOLLOWED,
  SEARCH_RESULTS_LOADED,
  ARTICLES_LOADED,
  ARTICLE_DELETED,
  ARTICLE_SAVED,
  ARTICLE_UNSAVED,
  ANSWER_DELETED,
  ANSWERS_LOADED,
  ANSWERS_UPDATED,
  ANSWER_SAVED,
  ANSWER_UNSAVED,
  QUESTIONS_LOADED,
  ACCOUNT_DELETED,
} from './constants/actionTypes';
import mixpanel from 'mixpanel-browser'
require('dotenv').config()

export const initialState = {
  user: null,
  profile: null,
  articles: [],
  answers: [],
  savedAnswers: [],
  savedPosts: [],
  questions: [],
  questionResults: [],
  profileResults: [],
  postResults: [],
  searchTerm: ''
};

mixpanel.init(process.env.MIXPANELTOKEN);

const reducer = (state, action) => {
  switch (action.type) {
    case APP_LOAD: //1
      return {
        ...state,
        profile: action.profile,
        user: action.user,
      }
    case SIGNUP_PAGE_LOADED:
      mixpanel.track("SignUp Page View");
      return {
        ...state
      }
    case SIGNUP_SUCCESS: //2
      mixpanel.alias(action.profile.email);
      mixpanel.track("User Sign Up Successful.", {
        "Signup method": "tiwik"
      });
      mixpanel.people.set({
        "$name": action.profile.full_name,
        "$email": action.profile.email,
      })
      mixpanel.identify(action.profile.email);
      return {
        ...state,
        user: action.user,
        profile: action.profile
      };
    case GOOGLE_SIGNUP_SUCCESS: //3
      mixpanel.alias(action.profile.email);
      mixpanel.track("User Sign Up Successful.", {
        "Signup method": "google"
      });
      mixpanel.people.set({
        "$name": action.profile.full_name,
        "$email": action.profile.email,
      })
      mixpanel.identify(action.profile.email);
      return {
        ...state,
        user: action.user,
        profile: action.profile
      };
    case GOOGLE_LOGIN_SUCCESS: //4
      mixpanel.identify(action.profile.email);
      mixpanel.track("Logged in", {
        "Login method": "google"
      });
      return {
        ...state,
        user: action.user,
        profile: action.profile
      };
    case LOGIN_SUCCESS: //5
      mixpanel.identify(action.profile.email);
      mixpanel.track("Logged in", {
        "Login method": "tiwik"
      });
      return {
        ...state,
        user: action.user,
        profile: action.profile,
        savedAnswers: action.savedAnswers,
        savedPosts: action.savedPosts
      };
    case LOGOUT_SUCCESS: //6
      mixpanel.track("Logged out");
      mixpanel.reset();
      return {
        ...state,
        user: action.user,
        profile: action.profile,
      };
    case PROFILE_UPDATED: //7
      return {
        ...state,
        profile: action.profile,
      }
    case PROFILE_FOLLOWED: //8
      return {
        ...state,
        profile: action.profile,
      }
    case PROFILE_UNFOLLOWED: //9
      return {
        ...state,
        profile: action.profile,
      }
    case ARTICLES_LOADED: //11
      return {
        ...state,
        articles: action.articles,
      }
    case ARTICLE_SUBMITTED: //12
      return {
        ...state,
        articles: action.articles,
      }
    case ARTICLE_DELETED: //13
      return {
        ...state,
        articles: action.articles,
      }
    case QUESTIONS_LOADED: //14
      return {
        ...state,
        questions: action.questions,
      }
    case SEARCH_RESULTS_LOADED: //15
      return {
        ...state,
        questionResults: action.questionResults,
        profileResults: action.profileResults,
        postResults: action.postResults,
        searchTerm: action.searchTerm
      }
    case ANSWERS_LOADED: //16
      return {
        ...state,
        answers: action.answers,
      }
    case ANSWERS_UPDATED: //17
      return {
        ...state,
        answers: action.answers,
      }
    case ANSWER_DELETED: //18
      return {
        ...state,
        answers: action.answers,
      }
    case ANSWER_SAVED: //19
      return {
        ...state,
        savedAnswers: action.savedAnswers,
      }
    case ANSWER_DELETED: //19
      return {
        ...state,
        savedAnswers: action.savedAnswers,
      }
    case ARTICLE_SAVED:
      return {
        ...state,
        savedPosts: action.savedPosts,
      }
    case ANSWER_UNSAVED: //19
      return {
        ...state,
        savedAnswers: action.savedAnswers,
      }
    case ARTICLE_UNSAVED:
      return {
        ...state,
        savedPosts: action.savedPosts,
      }
    case ACCOUNT_DELETED:
      mixpanel.track("Account Deleted");
      mixpanel.reset();
      return {
        ...state,
        user: action.user,
        profile: action.profile,
      }
    default:
      return state;
  }
};

export default reducer;