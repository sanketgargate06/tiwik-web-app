import React from 'react';
import ReactDOM from 'react-dom';
import { GlobalStateProvider } from './store';
import App from './components/App';
import reducer, { initialState } from './reducer';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render((
  <React.StrictMode>
    <GlobalStateProvider initialState={initialState} reducer={reducer}>
      <App />
    </GlobalStateProvider>
  </React.StrictMode>
  ),
  document.getElementById('root'));