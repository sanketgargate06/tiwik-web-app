import React, { useState } from 'react'
import Modal from "react-modal";
import { useStateValue } from "../../store";
import axios from "axios";
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./EditQuestion.css"
import { QUESTIONS_LOADED } from "../../constants/actionTypes";
import axiosInstance from '../../axiosApi';

function EditQuestion({ slug }) {
  const [{ user, questions }, dispatch] = useStateValue();
  const [modalIsOpen, setIsOpen] = useState(false);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  function openModal({ slug }) {
    setIsOpen(true);
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    axiosInstance.get(`/api/question/${slug}/`, config)
      .then(res => {
        setTitle(res.data.title)
        setDescription(res.data.description)
      })
  }

  function closeModal() {
    setIsOpen(false);
  }

  function handleSubmit({ slug }) {
    setIsOpen(false);
    const updatedQuestions = questions.filter(que => que["slug"] != slug)
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const body = {
      title: title,
      description: description,
    };
    console.log(body)
    axiosInstance.patch(`/api/question/${slug}/`, body, config)
      .then(res => {
        console.log(res.data)
        updatedQuestions.unshift(res.data)
        dispatch({
          type: QUESTIONS_LOADED,
          questions: updatedQuestions,
        })
      }).catch(err => {
        console.error(err)
      })
  }

  return (
    <>
      {!user ? "" :
        <div className="editQuestion">
          <Button variant="light" className="editQuestion__Button" onClick={() => openModal({ slug })} >
            <FontAwesomeIcon className="editquestion__editIcon" icon={faEdit} />
          </Button>

          <Modal
            isOpen={modalIsOpen}
            onRequestClose={closeModal}
            className="editQuestion__Modal"
            contentLabel="Example Modal"
          >
            {!title ? <div className="loader simple-circle"></div> :
              <form>
                <center>
                  <h3 className="editQuestion__header">Edit Question</h3>
                  <input
                    type="textarea"
                    className="editQuestion__title"
                    value={title}
                    onChange={e => setTitle(e.target.value)}
                  />
                  <textarea
                    className="editQuestion__description"
                    placeholder="Describe your problem in detail..(Optional)"
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                  />
                  <Button onClick={() => handleSubmit({ slug })} variant="primary">Ask</Button>{' '}

                  <Button onClick={closeModal} variant="danger">Cancel</Button>{' '}
                </center>
              </form>}
          </Modal>
        </div>}
    </>
  )
}

export default EditQuestion
