import React from "react";
import moment from "moment";

function DatePublished({ date }) {
  return (
    <small className="datePublished mr-4">
      <b>{moment(date).format("DD MMM, YYYY")}</b>
    </small>
  );
}

export default DatePublished;
