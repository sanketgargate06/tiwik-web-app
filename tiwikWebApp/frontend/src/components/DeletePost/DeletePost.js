import React from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import "./DeletePost.css";
import Button from 'react-bootstrap/Button'
import axiosInstance from '../../axiosApi';
import { useStateValue } from '../../store';
import { ARTICLE_DELETED, ANSWER_DELETED } from "../../constants/actionTypes";


function DeletePost({ type, qid, slug }) {
  const [{ articles, answers }, dispatch] = useStateValue()

  function deleteQuestion() {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    axiosInstance.delete(`/api/${type}/${qid}/`)
      .then(res => {
        if (res.status == 204) {
          if (type = "answers") {
            const updatedAnswers = answers.filter(answer => answer["id"] != qid)
            dispatch({
              type: ANSWER_DELETED,
              answers: updatedAnswers
            })
          }
          if (type = "posts") {
            const updatedArticles = articles.filter(article => article["id"] != qid)
            dispatch({
              type: ARTICLE_DELETED,
              articles: updatedArticles
            })
          }
        }
      })
  }


  return (
    <Button variant="light" className="deleteButton" onClick={() => deleteQuestion(type, qid)}>
      <FontAwesomeIcon icon={faTrash} className="deletePost__Icon" />
    </Button>
  )
}

export default DeletePost
