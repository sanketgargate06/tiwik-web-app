import React from "react";
import { Link } from "react-router-dom";

function PostTitle({ title, slug, type }) {
  return (
    <div>
      {type == "answers" ?
        <Link to={`/discuss/${slug}`} className="article__title">
        {title}
        </Link> :
        <h5 className="article__title">{title}</h5>}
    </div>
  );
}

export default PostTitle;
