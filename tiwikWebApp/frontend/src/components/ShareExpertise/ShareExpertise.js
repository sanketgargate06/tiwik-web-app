import React, { useState } from "react";
import "./ShareExpertise.css";
import InlineEditor from '@ckeditor/ckeditor5-build-inline';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import axiosInstance from "../../axiosApi";
import Modal from "react-modal";
import { useHistory } from "react-router";
import { Button } from "react-bootstrap";
import Alert from 'react-bootstrap/Alert'
import { useStateValue } from "../../store";
import { ARTICLE_SUBMITTED } from "../../constants/actionTypes";

function ShareExpertise() {
  const [{ user, articles }, dispatch] = useStateValue();
  const history = useHistory();
  const [text, setText] = useState('');
  const [title, setTitle] = useState('');
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const config = {
    toolbar: ['bold', 'italic', 'Link', '|', 'Undo', 'Redo']
  };

  function handleSubmit() {

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const body = {
      postTitle: title,
      postContent: text,
    }

    axiosInstance.post('/api/posts/', body, config)
      .then(res => {
        setModalIsOpen(false);
        setText('')
        setTitle('')
        articles.unshift(res.data)
        dispatch({
          type: ARTICLE_SUBMITTED,
          articles: articles,
        })
      })
    }

  return (
    <div className="share">
      <input className="share__select" type="text" placeholder={`Share Your Story, ${user.first_name}`} onClick={() => setModalIsOpen(true)} />
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={() => setModalIsOpen(false)}
        className="share__Modal"
        contentLabel="Example Modal"
      >
        {showAlert ?
          <Alert variant="danger" className="share__alert" onClose={() => setShowAlert(false)} dismissible>
            <h6 className="share__alertText">
              An Error Occured! You might have Post with Same name
          </h6>
          </Alert> : ""
        }

        <form onSubmit={handleSubmit}>
          <div className="share__header">Write Post</div>
          <div className="share__titleWrapper">
            <h5>Title</h5>
          <input
              type="textarea"
              className="share__title"
              value={title}
              onChange={e => setTitle(e.target.value)}
            />
          </div>
          <div className="share__content">
            <h5>Content</h5>
          <CKEditor
              editor={InlineEditor}
              data={text}
              config={config}
              placeholder="text"
            onChange={(event, editor) => {
              const data = editor.getData();
              setText(data);
              // console.log({ event, editor, data });
            }} />
        </div>
        <center>
            <Button variant="primary" onClick={handleSubmit}>Submit</Button>
            <Button variant="danger" onClick={() => setModalIsOpen(false)}>Cancel</Button>
        </center>
      </form>
      </Modal>
    </div>
  );
}

export default ShareExpertise;

