import React, { useEffect, useState } from 'react';
import { Tab, Tabs } from 'react-bootstrap';
import "./BookMarked.css";
import { faBookOpen, faScroll } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useStateValue } from '../../store';
import Post from '../Post/Post';
import { useParams } from 'react-router';
import axiosInstance from '../../axiosApi';
import { ANSWER_SAVED, ARTICLE_SAVED } from "../../constants/actionTypes";

function BookMarked() {
  const [{ profile, savedPosts, savedAnswers }, dispatch] = useStateValue();
  const [key, setKey] = useState('savedarticles');
  const { slug } = useParams();

  function getProfile(slug) {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    axiosInstance.get(`/user/profile/${slug}`, config)
      .then(res => {
        dispatch({
          type: ANSWER_SAVED,
          savedAnswers: res.data.savedAnswers
        })
        dispatch({
          type: ARTICLE_SAVED,
          savedPosts: res.data.savedPosts
        })
      })
  }

  useEffect(() => {
    getProfile(slug);
  }, []);

  return (
    <div className="bookMarked">
      <center>
        <span className="bookMarked__header">Your Bookmarks</span>
      </center>
      <Tabs justify id="controlled-tab-example" activeKey={key} onSelect={(k) => setKey(k)}>
        <Tab eventKey="savedarticles" title={<FontAwesomeIcon icon={faBookOpen} className="tabMenuIcon" />} >
          {savedPosts ?
            <div className="bookmarked_Articles">
              {savedPosts.length > 0 ?
                <div>
                  <h4 className="bookmarked_ArticlesHeader">Articles</h4>
                  {
                    savedPosts.map((article) => (
                      <Post
                        key={article.id}
                        id={article.id}
                        type="posts"
                        author_slug={article.author_slug}
                        slug={article.slug}
                        author={article.author}
                        title={article.postTitle}
                        content={article.postContent}
                        profilePic={article.profilePic}
                        published_on={article.postPublished}
                        likes={article.savedCount}
                        is_saved={article.is_saved}
                      />
                    ))
                  }
                </div> : ""
              }
            </div> : <h6 className="no__articles">You don't have any saved articles</h6>
          }
        </Tab>
        <Tab eventKey="savedanswers" title={<FontAwesomeIcon icon={faScroll} className="tabMenuIcon" />}>
          {savedAnswers ?
            <div className="bookmarked_Answers">
              <h4 className="bookmarked_ArticlesHeader">Answers</h4>
              {
                savedAnswers.map((answer) => (
                  <Post
                    key={answer.id}
                    id={answer.id}
                    type="answers"
                    author_slug={answer.author_slug}
                    slug={answer.slug}
                    author={answer.author}
                    title={answer.title}
                    content={answer.content}
                    profilePic={answer.profilePic}
                    published_on={answer.published_on}
                    likes={answer.savedCount}
                    is_saved={answer.is_saved}
                  />
                )
                )
              }</div> :
            <h6 className="no__articles">You don't have any saved answers</h6>
          }
        </Tab>
      </Tabs>
    </div>
  )
}

export default BookMarked
