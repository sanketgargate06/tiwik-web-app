import React, { useEffect, useState } from 'react'
import { useParams } from "react-router-dom";
import { useStateValue } from '../../store';
import Post from '../Post/Post';
import axios from "axios";
import './PostDetail.css';
import { ANSWERS_LOADED } from '../../constants/actionTypes';
import EditPost from '../EditPost/EditPost';
import axiosInstance from '../../axiosApi';

function PostDetail() {
  const [{ user, answers }, dispatch] = useStateValue();
  const { slug } = useParams();
  const [Loading, setLoading] = useState("false");
  const [title, setTitle] = useState('');
  const [qid, setQid] = useState();
  const [description, setDescription] = useState('');
  const [readMore, setReadMore] = useState(false);

  const getQuestion = () => {
    setLoading("true")
    axiosInstance.get(`/api/question/${slug}/`)
      .then(res => {
        setTitle(res.data.title)
        setQid(res.data.id)
        setDescription(res.data.description)
      })
      .catch((err) => {
        console.log(err)
        setLoading("true")
      });
  }

  const getFeed = () => {
    setLoading("true")
    axiosInstance.get('/api/answers/')
      .then(res => {
        dispatch({
          type: ANSWERS_LOADED,
          answers: res.data
        })
      })
      .catch((err) => {
        console.log(err)
        setLoading("true")
      });
  }

  useEffect(() => {
    getQuestion();
    getFeed();
  }, []);


  return (
    <div>
      {title ? <>
        <h4 className="postdetail__title">{title}</h4>
        {description ?
          <p className="postdetail__description">
            {readMore ? description : description.slice(0, 200)}
            {description.length > 200 ?
              <span onClick={() => setReadMore(!readMore)}
                className="answer__readMore">{readMore ? "...Read Less" : " ...Read More"}
              </span>
              : ""}
          </p> : ""
        }
        <div className="postdetail__add">
          <span>Share Something, {user ? user.first_name : ""} </span>
          <EditPost type="answers" qid={qid} slug={slug} title={title} />
        </div>
        {answers.filter(questionPost => questionPost.slug == slug).map((post) => (
        <Post
          key={post.id}
          id={post.id}
          author_slug={post.author_slug}
            type="answers"
            slug={post.slug}
            author={post.author}
          content={post.content}
          profilePic={post.profilePic}
          published_on={post.published_on}
            likes={post.savedCount}
            is_saved={post.is_saved}
        />
      ))}
      </> : ""}
    </div>
  )
}

export default PostDetail
