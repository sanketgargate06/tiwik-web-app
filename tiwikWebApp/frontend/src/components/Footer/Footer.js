import React from 'react';
import { Container, Nav, Navbar, NavItem } from 'react-bootstrap';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { NavLink } from 'react-router-dom';
import { faFacebook, faTwitter, faLinkedin, faInstagram, faPinterest } from "@fortawesome/free-brands-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import './Footer.css';

function Footer() {
  return (
    <footer className="page-footer center">
      <Container>
        <Row className="footer-links">
          <Col md={2}>
            <h6>
              <NavLink className="f-link" to="/about">
                  About
              </NavLink>
            </h6>
          </Col>
          <Col md={2}>
            <h6>
              <NavLink className="f-link" to="/terms">
                Terms
                </NavLink>
            </h6>
          </Col>
          <Col md={2}>
            <h6>
              <NavLink className="f-link" to="/privacy">
                Privacy Policy
                </NavLink>
            </h6>
          </Col>
          <Col md={2}>
            <h6>
              <NavLink className="f-link" to="/cookies">
                Cookie Policy
                  </NavLink>
            </h6>
          </Col>
          <Col md={2}>
            <h6>
              <NavLink className="f-link" to="/support">
                <span>Support</span>
              </NavLink>
            </h6>
          </Col>
        </Row>
        <Row className="text-center justify-content-center mb-md-0">
          <Col md={8} xs={12}>
            {/* <div className="footer-socials">
              <Link to="#"><FontAwesomeIcon className="fa-lg" icon={faFacebook} /> </Link>
              <Link to="#"><FontAwesomeIcon className="fa-lg" icon={faTwitter} /> </Link>
              <Link to="#"><FontAwesomeIcon className="fa-lg" icon={faLinkedin} /> </Link>
              <Link to="#"><FontAwesomeIcon className="fa-lg" icon={faInstagram} /> </Link>
              <Link to="#"><FontAwesomeIcon className="fa-lg" icon={faPinterest} /> </Link>
            </div> */}
            <div className="mb-4">
              <Container fluid>
                © 2021 Copyright: <span> tiwik.in </span>
              </Container>
                </div>
          </Col>
        </Row>
      </Container>
        </footer> 

  )
}

export default Footer
