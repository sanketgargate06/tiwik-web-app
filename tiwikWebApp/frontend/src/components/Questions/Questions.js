import React, { useEffect, useState } from 'react';
import "./Questions.css";
import Question from '../Question/Question';
import { useStateValue } from "../../store";
import { QUESTIONS_LOADED } from "../../constants/actionTypes";
import AskAdvice from "../AskAdvice/AskAdvice";
import axiosInstance from '../../axiosApi';

function Questions() {
  const [loading, setLoading] = useState("false");
  const [{ questions }, dispatch] = useStateValue();

  const getQuestions = () => {
    setLoading("true")
    axiosInstance.get("/api/question/")
      .then(res => {
        dispatch({
          type: QUESTIONS_LOADED,
          questions: res.data,
        })
      })
      .catch((err) => {
        console.log(err)
        setLoading("true")
      });
  }

  useEffect(() => {
    getQuestions();
  }, []);


  return (
    <div className="questions">
      <AskAdvice />
      <h4 className="questions__header">Latest Queries</h4>
      <div className="questions__list">
        {questions.map((question) => (
          <Question
            key={question.slug}
            id={question.id}
            title={question.title}
            description={question.description}
            questionAuthorSlug={question.question_author_slug}
            slug={question.slug} />
        ))}
      </div>
    </div>
  )
}

export default Questions
