import React, { useEffect, useState } from "react";
import { GoogleLogin } from "react-google-login";
import "./Register.css";
import { Link, useHistory } from "react-router-dom";
import { useStateValue } from "../../store";
import { GOOGLE_SIGNUP_SUCCESS, SIGNUP_PAGE_LOADED, SIGNUP_SUCCESS } from "../../constants/actionTypes";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Button from 'react-bootstrap/Button'
import axiosInstance from '../../axiosApi';
import { set } from 'idb-keyval';
import axios from 'axios';
require('dotenv').config()

const clientID = process.env.CLIENTID

function Register() {
  const history = useHistory();
  const [state, dispatch] = useStateValue();
  const [email, setEmail] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [password, setPassword] = useState('');
  const [passwordShown, setPasswordShown] = useState(false);

  const togglePasswordVisiblity = () => {
    setPasswordShown(!passwordShown);
  };

  useEffect(() => {
    function PageLoad() {
      dispatch({
        type: SIGNUP_PAGE_LOADED,
      })
      console.log("Page View");
    }
    PageLoad();
  }, []);

  const handleSubmit = e => {
    e.preventDefault();
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const body = {
      email: email,
      first_name: firstName,
      last_name: lastName,
      password1: password,
      password2: password
    };

    axios.post('/dj-rest-auth/registration/', body, config)
      .then(res => {
        if (res.data.user) {
          axiosInstance.defaults.headers['Authorization'] = "JWT " + res.data.access_token;
          localStorage.setItem('access_token', JSON.stringify(res.data.access_token));
          localStorage.setItem('refresh_token', JSON.stringify(res.data.refresh_token));
          set('user', res.data.user)
          set('profile', res.data.user.profile)
          history.push('/');
        }
        dispatch({
          type: SIGNUP_SUCCESS,
          user: res.data.user,
          profile: res.data.user.profile,
        })
      })
      .catch((err) => {
        console.log(err)
      });
  }

  return (
    <div className="signup">
      <center>
        <div className="signup__box">
          <h2>Create your Tiwik account</h2>
          <form>
            <input
              type="email"
              className="signup__email"
              placeholder="Email"
              value={email}
              onChange={e => setEmail(e.target.value)}
            />
            <input
              type="text"
              className="signup__firstName"
              placeholder="First name"
              value={firstName}
              onChange={e => setFirstName(e.target.value)}
            />
            <input
              type="text"
              className="signup__surName"
              placeholder="Surname"
              value={lastName}
              onChange={e => setLastName(e.target.value)}
            />
            <div className="signup__wrap__password">
              <input
                type={passwordShown ? "text" : "password"}
                className="signup__password"
                placeholder="Password"
                value={password}
                onChange={e => setPassword(e.target.value)}
              />
              <FontAwesomeIcon onClick={togglePasswordVisiblity} className="eyeIcon" icon={passwordShown ? faEye : faEyeSlash} />
            </div>
            <Button onClick={handleSubmit} className="register__button">Sign Up</Button>
            <p className="text-muted">Already Have an Account ?</p>
            <Link to="/login" className="btn register__loginLink">
              Sign In
            </Link>
          </form>
        </div>
      </center>
    </div>
  );
}

export default Register;
