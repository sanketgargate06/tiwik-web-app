import React, { useState } from 'react';
import "./EditPost.css";
import Modal from "react-modal";
import { useStateValue } from '../../store';
import axiosInstance from "../../axiosApi";
import { faPenFancy } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import InlineEditor from '@ckeditor/ckeditor5-build-inline';
import { CKEditor } from '@ckeditor/ckeditor5-react'
import Button from 'react-bootstrap/Button';
import { useHistory } from "react-router";
import { ARTICLES_LOADED, ANSWERS_UPDATED } from "../../constants/actionTypes";

function EditPost({ type, qid, slug, title }) {
  const [modalIsOpen, setIsOpen] = useState(false);
  const [{ user, articles, answers }, dispatch] = useStateValue();
  const [text, setText] = useState('');
  const [OriginalText, setOriginalText] = useState(false);
  const [answerId, setAnswerId] = useState(0);
  const history = useHistory();

  const config = {
    toolbar: ['bold', 'italic', 'Link', '|', 'Undo', 'Redo']
  };

  function openModal({ type, slug, user }) {
    setIsOpen(true);
    if (type == "posts") {
      const editpost = articles.find(post => post["slug"] == slug && post["author_slug"] == user.profile["url_slug"])
      setText(editpost["postContent"])
      setOriginalText(true)
    }
    if (type == "answers") {
      const editanswer = answers.filter(answer => answer["slug"] == slug && answer["author_slug"] == user.profile["url_slug"])
      if (editanswer[0]) {
        setText(editanswer[0]["content"])
        setAnswerId(editanswer[0]["id"])
        setOriginalText(true)
      }
    }
  }

  function savePost({ type }) {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    if (type == "posts") {
      const body = {
        postContent: text
      }
      axiosInstance.patch(`/api/${type}/${qid}/`, body, config)
        .then(res => {
          console.log(`/api/${type}/${qid}/`)
          const updatedArticles = articles.filter(article => article["id"] != qid)
          updatedArticles.unshift(res.data)
          dispatch({
            type: ARTICLES_LOADED,
            articles: updatedArticles
          })
        })
      setIsOpen(false);
    }
    else if (type = "answers") {
      const body = {
        title: title,
        content: text
      }
      axiosInstance.patch(`/api/${type}/${answerId}/`, body, config)
        .then(res => {
          const updatedAnswers = answers.filter(answer => answer["id"] != answerId)
          updatedAnswers.unshift(res.data)
          dispatch({
            type: ANSWERS_UPDATED,
            answers: updatedAnswers
          })
        })
      history.push(`/discuss/${slug}`);
      setIsOpen(false);
    }
  }

  function handleSubmit({ slug }) {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const body = {
      title: title,
      answers: [
        {
          content: text
        }
      ]
    }

    axiosInstance.patch(`/api/question/${slug}/`, body, config)
      .then(res => {
        console.log(answers)
        const updatedAnswers = res.data.answers.concat(answers.filter(answer => answer["slug"] != slug)).sort((a, b) => b.published_on - a.published_on)
        const sortedAnswers = updatedAnswers.sort((a, b) => new Date(b.published_on) - new Date(a.published_on))
        console.log(sortedAnswers)
        dispatch({
          type: ANSWERS_UPDATED,
          answers: sortedAnswers
        })
      })
    setIsOpen(false);
  }


  return (
    <div className="editPost">
      <Button variant="light" className="answerButton" onClick={() => openModal({ type, slug, user })}>
        <FontAwesomeIcon className="editPost__answerIcon" icon={faPenFancy} />
      </Button>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={() => setIsOpen(false)}
        className="editPost__answerModal"
        contentLabel="Example Modal"
      >
        <form>
          <center>
            <div className="editPost__answerTitle">{title}</div>
            <CKEditor
              editor={InlineEditor}
              data={text}
              config={config}
              onChange={(event, editor) => {
                const data = editor.getData();
                setText(data);
              }} />
            {OriginalText ?
              <Button onClick={() => savePost({ type })} variant="success">Save</Button> :
              <Button onClick={() => handleSubmit({ slug })} variant="success">Submit</Button>
            }
            <Button onClick={() => setIsOpen(false)} variant="danger">Cancel</Button>{' '}
          </center>
        </form>
      </Modal>
    </div>
  )
}
export default EditPost
