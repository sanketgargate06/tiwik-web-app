import React, { useState } from 'react';

import './PostEditor.css';

function PostEditor() {

  return (
    <div>
      <CKEditor
        editor={ClassicEditor}
        data={text}
        placeholder="text"
        onChange={(event, editor) => {
          const data = editor.getData();
          console.log({ event, editor, data });
        }} />
      <div className="content">
        <h2>Content</h2>
        <p>{text}</p>
      </div>
    </div>
  )
}

export default PostEditor
