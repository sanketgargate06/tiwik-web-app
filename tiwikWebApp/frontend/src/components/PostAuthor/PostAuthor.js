import React from "react";
import { Badge } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { useStateValue } from "../../store";
import "./PostAuthor.css";

function PostAuthor({ author, author_slug }) {
  const [{ user }, dispatch] = useStateValue();

  return ( 
    <div>
      <span className="text-muted">
        by </span>
      <Badge pill className="article__authorWrapper">
      <NavLink to={user ? `/profile/${author_slug}` : "/login"}
        className="article__author">
        {author}
      </NavLink>
      </Badge>

    </div> 
  );
}

export default PostAuthor;
