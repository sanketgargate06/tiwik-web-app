import React, { useEffect, useState } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch, } from "react-router-dom";
import Feed from "./Feed/Feed";
import Header from "./Header/header";
import Login from "./Login/Login";
import Profile from "./Profile/Profile";
import ShareExpertise from "./ShareExpertise/ShareExpertise";
import Register from "./Register/Register";
import { useStateValue } from "../store";
import { APP_LOAD } from "../constants/actionTypes";
import Row from 'react-bootstrap/Row'
import { Col, Container } from "react-bootstrap";
import Landing from "./Landing/Landing";
import Cookies from "./About/Cookies";
import Privacy from "./About/Privacy";
import About from "./About/About";
import Support from "./About/Support";
import Footer from "./Footer/Footer";
import Terms from "./About/Terms";
import PostDetail from "./PostDetail/PostDetail";
import ResetPassword from "./ResetPassword/ResetPassword";
import ResetPasswordSent from "./ResetPassword/ResetPasswordSent";
import ResetPasswordConfirm from "./ResetPassword/ResetPasswordConfirm";
import ResetPasswordDone from "./ResetPassword/ResetPasswordDone";
import Results from "./Results/Results";
import Connection from "./Connection/Connection";
import { get } from 'idb-keyval';
import BookMarked from "./BookMarked/BookMarked";
import Settings from "./Settings/Settings";
import CookieBanner from "./CookieBanner/CookieBanner";

function App(props) {
  const [{ user, profile }, dispatch] = useStateValue();
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function fetchData() {
      const loggedInProfile = await get('profile')
      const loggedInUser = await get('user')

      dispatch({
        type: APP_LOAD,
        profile: loggedInProfile,
        user: loggedInUser
      })
    }
    fetchData();
  }, []);

  return (
    <Router>
      <div className="app">
        <Switch>
          <Route exact path="/">
          <Header />
            <CookieBanner />
            {user ? 
            <main role="main" className="container">
              <Row>
                  <Col md={3} className="show_connections">
                    <Connection user={user} />
                  </Col>
                  <Col md={9} className="show_feed">
                    <Feed />
                  </Col>
              </Row>
            </main>
              :
              <main>
                <Landing />
              </main>
            }
          </Route>
          <Route exact path="/create">
            <Header />
            <CookieBanner />
            <main role="main" className="container">
              <Row>
                <Col md={10}>
                  <ShareExpertise />
                </Col>
              </Row>
            </main>
          </Route>
          <Route exact path="/:slug/saved">
            <Header />
            <CookieBanner />
            <main role="main" className="container">
              <Row>
                <Col md={10}>
                  <BookMarked />
                </Col>
              </Row>
            </main>
          </Route>
          <Route exact path="/discuss/:slug">
            <Header /><CookieBanner />
            <main role="main" className="container">
              <Row>
                <Col md={10}>
                  <PostDetail />
                </Col>
              </Row>
            </main>
          </Route>
          <Route exact path="/profile/:slug">
            <Header /><CookieBanner />
            <main role="main" className="container">
              <Row>
                <Col md={10}>
                  <Profile />
                </Col>
              </Row>
            </main>
          </Route>
          <Route exact path="/login">
            <Header /><CookieBanner />
            <Login />
            <Footer />
          </Route>
          <Route exact path="/register">
            <Header /><CookieBanner />
            <Register />
            <Footer />
          </Route>
          <Route exact path="/about">
            <Header /><CookieBanner />
            <main>
              <About />
            </main>
            <Footer />
          </Route>
          <Route exact path="/terms">
            <Header /><CookieBanner />
            <main>
              <Terms />
            </main>
            <Footer />
          </Route>
          <Route exact path="/privacy">
            <Header /><CookieBanner />
            <main>
              <Privacy />
            </main>
            <Footer />
          </Route>
          <Route exact path="/cookies">
            <Header /><CookieBanner />
            <main>
              <Cookies />
            </main>
            <Footer />
          </Route>
          <Route exact path="/support">
            <Header /><CookieBanner />
            <main>
              <Support />
            </main>
            <Footer />
          </Route>
          <Route exact path="/password/reset">
            <Header /><CookieBanner />
            <main role="main" className="container">
              <Row>
                <Col md={10}>
                  <ResetPassword />
                </Col>
              </Row>
            </main>
          </Route>
          <Route exact path="/password/reset/sent">
            <Header /><CookieBanner />
            <main role="main" className="container">
              <Row>
                <Col md={10}>
                  <ResetPasswordSent />
                </Col>
              </Row>
            </main>
          </Route>
          <Route exact path="/password/reset/:uidb64/:token/">
            <Header /><CookieBanner />
            <main role="main" className="container">
              <Row>
                <Col md={10}>
                  <ResetPasswordConfirm />
                </Col>
              </Row>
            </main>
          </Route>
          <Route exact path="/password/reset/done">
            <Header /><CookieBanner />
            <main role="main" className="container">
              <Row>
                <Col md={10}>
                  <ResetPasswordDone />
                </Col>
              </Row>
            </main>
          </Route>
          <Route exact path="/results">
            <Header /><CookieBanner />
            <main role="main" className="container">
              <Row>
                <Col md={10}>
                  <Results />
                </Col>
              </Row>
            </main>
          </Route>
          <Route exact path="/:slug/settings">
            <Header /><CookieBanner />
            <main role="main" className="container">
              <Row>
                <Col md={10}>
                  <Settings />
                </Col>
              </Row>
            </main>
          </Route>
        </Switch>
      </div>
    </Router >
  );
}

export default App;
