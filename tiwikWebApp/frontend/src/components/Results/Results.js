import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useStateValue } from '../../store';
import "./Results.css";
import Image from 'react-bootstrap/Image';
import Modal from "react-modal";
import axios from 'axios';
import { post } from 'superagent';
import parse from 'html-react-parser';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import Button from 'react-bootstrap/Button'

function Results() {
  const [{ questionResults, profileResults, postResults, searchTerm }, dispatch] = useStateValue();
  const [modalIsOpen, setIsOpen] = useState(false);
  const [openTitle, setOpenTitle] = useState('');
  const [openContent, setOpenContent] = useState('');

  function openModal(id, postTitle, postContent) {
    setIsOpen(true);
    setOpenTitle(postTitle);
    setOpenContent(parse(postContent));
  }

  function closeModal() {
    setOpenTitle('');
    setOpenContent('');
    setIsOpen(false)
  }

  return (
    <div className="results">
      <div className="results__header">Search Results For
      <span className="results__searchTerm"> {searchTerm}</span>
      </div>
      {questionResults.length > 0 ?
        <div className="results__wrapper">
          <h4 className="results__Subheader">Discussions</h4>
          {questionResults.map((question) => (
            <span className="results__title">
              <Link to={`/discuss/${question.slug}`} key={question.id}>{question.title}</Link>
            </span>
          ))}
        </div>
        : ""
      }
      {postResults.length > 0 ?
        <div className="results__wrapper">
          <h4 className="results__Subheader">Posts</h4>
          {postResults.map((post) => (
            <span key={post.id} onClick={() => openModal(post.id, post.postTitle, post.postContent)} className="results__title">
              {post.postTitle}</span>
          ))}
          <Modal
            isOpen={modalIsOpen}
            onRequestClose={() => setIsOpen(false)}
            className="results__Postmodal"
            contentLabel="Example Modal"
            overlayClassName="Overlay"
          >
            <div className="results__openPost">
              <div className="results__openPostHeader">
                <div className="results__openPostButtonWrapper">
                  <Button variant="light" className="results__closeButton" onClick={() => closeModal()}>
                    <FontAwesomeIcon icon={faTimes} />
                  </Button>
                </div>
                <h4 className="results__postTitle">{openTitle}</h4>
              </div>
              <div className="results__postContent">{openContent}</div>
            </div>
          </Modal>
        </div> : ""
      }
      {profileResults.length > 0 ?
        <div className="results__wrapper">
          <h4 className="results__Subheader">Profiles</h4>
          {profileResults.map((profile) => (
            <Link to={`/profile/${profile.url_slug}`} target="_blank" key={profile.url_slug} className="profileResults__profile">
              <Image className="results__profileImg" src={profile.image} alt="" roundedCircle />
              <span className="profileResults__title">{profile.full_name}</span>
              {profile.user_bio ?
                <span className="profileResults__bio">{profile.user_bio.length > 50 ? <>{profile.user_bio.slice(0, 60).concat('...')} </> : profile.user_bio}</span> : ""}
            </Link>
          ))}
        </div> : ""}
    </div>
  )
}

export default Results
