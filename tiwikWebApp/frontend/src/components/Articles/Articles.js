import React, { useEffect } from 'react';
import { useStateValue } from "../../store";
import { ARTICLES_LOADED } from "../../constants/actionTypes";
import Post from '../Post/Post';
import axiosInstance from '../../axiosApi';

function Articles() {
  const [{ articles }, dispatch] = useStateValue();

  const getArticles = () => {
    axiosInstance.get("/api/posts/")
      .then(res => {
        dispatch({
          type: ARTICLES_LOADED,
          articles: res.data
        })
      })
      .catch((err) => {
        console.log(err)
      });
  }

  useEffect(() => {
    getArticles();
  }, []);


  return (
    <>
    {articles ?
      <div>
      {articles.map((article) => (
        <Post
          key={article.id}
          id={article.id}
          type="posts"
          author_slug={article.author_slug}
          slug={article.slug}
          author={article.author}
          title={article.postTitle}
          content={article.postContent}
          profilePic={article.profilePic}
          published_on={article.postPublished}
          likes={article.savedCount}
          is_saved={article.is_saved}
        />
      )
      )}
      </div> : ""}
    </>
  )
}

export default Articles
