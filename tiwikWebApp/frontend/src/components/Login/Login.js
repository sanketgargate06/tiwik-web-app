import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import "./Login.css";
import { GoogleLogin } from "react-google-login";
import { useStateValue } from "../../store";
import { GOOGLE_LOGIN_SUCCESS, LOGIN_SUCCESS } from "../../constants/actionTypes";
import { faEye, faEyeSlash, faExclamationCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from 'axios';
import axiosInstance from '../../axiosApi';
import { withRouter } from 'react-router-dom';
import { Button } from "react-bootstrap";
import Alert from 'react-bootstrap/Alert'
import { set } from 'idb-keyval';
require('dotenv').config()

const clientID = process.env.CLIENTID

function Login() {
  const history = useHistory();
  const [showAlert, setShowAlert] = useState(false);
  const [{ user, profile }, dispatch] = useStateValue();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordShown, setPasswordShown] = useState(false);
  const [warning, setWarning] = useState('');

  const togglePasswordVisiblity = () => {
    setPasswordShown(!passwordShown);
  };

  function handleSubmit(e) {
    e.preventDefault();
    const body = { email: email, password: password };
    axios
      .post('/api/token/obtain/', body)
      .then(response => {
        axiosInstance.defaults.headers['Authorization'] = "JWT " + response.data.access;
        localStorage.setItem('access_token', JSON.stringify(response.data.access));
        localStorage.setItem('refresh_token', JSON.stringify(response.data.refresh));
        axiosInstance
          .get('/dj-rest-auth/user/')
          .then(res => {
            set('user', res.data)
            set('profile', res.data.profile)
            dispatch({
              type: LOGIN_SUCCESS,
              user: res.data,
              profile: res.data.profile,
              savedAnswers: res.data.profile.savedAnswers,
              savedPosts: res.data.profile.savedPosts,
            })
            history.push("/");
          })
      }
    ).catch(error => {
      if (error.response.data.detail == "No active account found with the given credentials") {
        setWarning("Incorrect Email or Password");
      }
      else if (error.response.data.detail == "Too many failed login attempts") {
        setShowAlert(true);
        console.log('Locked')
      }
      throw error;
    }); 
  }


  return (
    <div className="login">
      <center>
        {showAlert ?
          <Alert variant="danger" className="login__alert" onClose={() => setShowAlert(false)} dismissible>
            <h6 className="login__alertText">
              Your account has been locked due to  multiple failed Login Attempts. Please try again after 30 minutes.
          </h6>
          </Alert> : ""
        }

        <div className="login__box">
          <h2>Log In to your Account</h2>
          {warning ? (<><FontAwesomeIcon icon={faExclamationCircle} className="login__errorSign" /><span className="login__error">{warning}</span></>) : ""}
          <form>
            <input
              type="email"
              className="login__email"
              value={email}
              placeholder="Email"
              onChange={e => setEmail(e.target.value)}
            />
            <div className="login__wrap__password">
              <input
                type={passwordShown ? "text" : "password"}
                className="login__password"
                placeholder="Password"
                value={password}
                onChange={e => setPassword(e.target.value)}
              />
              <FontAwesomeIcon onClick={togglePasswordVisiblity} className="eyeIcon" icon={passwordShown ? faEye : faEyeSlash} />
            </div>
            <Button onClick={handleSubmit} className="login__button">Log in</Button>
            <Link to="/password/reset" className="login__forget__password">
              Forgot Password ?
            </Link>
            <Link to="/register" className="btn login__RegisterLink">
              Create New Account
            </Link>
          </form>
        </div>
      </center>
    </div>
  );
}

export default withRouter(Login);
