import React, { useState } from 'react'
import { Button } from "react-bootstrap";
import axiosInstance from "../../axiosApi";
import { Link } from 'react-router-dom';
import Image from 'react-bootstrap/Image'
import mixpanel from 'mixpanel-browser'
import { useStateValue } from '../../store';
require('dotenv').config()

function ConnectProfile({ is_following, slug, image, name }) {
  const [{ user, profile }, dispatch] = useStateValue();
  const [isfollowing, setIsFollowing] = useState(is_following)
  mixpanel.init(process.env.MIXPANEL_TOKEN);

  const followProfile = (slug) => {
    if (isfollowing) {
      axiosInstance.delete(`/user/profile/${slug}/follow`)
        .then(res => {
          mixpanel.track("Unfollowed profile", {
            "follower": user.profile.full_name,
            "following": name
          })
          setIsFollowing(res.data.is_following)
        })
        .catch((err) => {
          console.log(err)
        });
    }
    else {
      axiosInstance.post(`/user/profile/${slug}/follow`)
        .then(res => {
          mixpanel.track("followed profile", {
            "follower": user.profile.full_name,
            "following": name
          })
          setIsFollowing(res.data.is_following)
        })
        .catch((err) => {
          console.log(err)
        });
    }
  }


  return (
    <div className="connection__profile">
      <Image className="connection__profileImg" src={image} alt="" roundedCircle />
      <div className="connection__profileInfo ">
        <Link to={`/profile/${slug}`} className="connection__ProfileName">{name}</Link>
        <Button size="sm" className={isfollowing ? "connection__following" : "connection__follow"} onClick={() => followProfile(slug)}>
          {isfollowing ? "Following" : "Follow"}
        </Button>
      </div>
    </div>
  )
}

export default ConnectProfile
