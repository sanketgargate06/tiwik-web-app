import React, { useEffect, useState } from 'react';
import "./Connection.css";
import axiosInstance from "../../axiosApi";
import ConnectProfile from './ConnectProfile';


function Connection({ user }) {
  const [profiles, setProfiles] = useState([]);
  const [randomProfiles, setRandomProfiles] = useState([]);

  function getProfiles() {
    axiosInstance.get('user/profile/')
      .then(res => {
        setProfiles(res.data)
        const filteredProfiles = res.data.filter(item => item["url_slug"] != 'admin-test-1' && item["is_following"] == false && item["url_slug"] != user.profile["url_slug"])
        var shuffle = require('shuffle-array');
        setRandomProfiles(shuffle.pick(filteredProfiles, { 'picks': 3 }));
      })
  }

  useEffect(() => {
    getProfiles();
  }, []);

  return (
    <div className="connection">
      <span className="connection__header">Find Connections</span>
      <div className="connection__wrapper">
        {randomProfiles.map(connection => (
          <ConnectProfile key={connection.url_slug} image={connection.image} name={connection.full_name} follow={connection.is_following} slug={connection.url_slug} />
        ))}
      </div>
    </div>
  )
}

export default Connection
