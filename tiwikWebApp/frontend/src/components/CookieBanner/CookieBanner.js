import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { NavLink } from 'react-router-dom';
import './CookieBanner.css';

function Cookies() {
  const history = useHistory();
  const [cbd, setCbd] = useState();

  useEffect(() => {
    if (localStorage.getItem("_cbd") === null) {
      setCbd(false);
    }
    else {
      setCbd(true)
    }
  }, []);


  const acceptCookie = () => {
    localStorage.setItem("_cbd", true)
    setCbd(localStorage.getItem("_cbd"));
  }

  return (
    <div className={cbd ? "cookie-container" : "cookie-container active"}>
      < p className="cookie-text" > This Website uses cookies to give you the best experience on our site.By using our site, you acknowledge that you have read and understand our  <NavLink className="banner_links" to="/cookies">Cookie Policy</NavLink>,   <NavLink className="banner_links" to="/privacy">Privacy Policy</NavLink>, and our   <NavLink className="banner_links" to="/terms">
        Terms of Service</NavLink>.
        < button className="cookie-btn" onClick={acceptCookie} > OK</button >
      </p >
    </ div >
  )
}

export default Cookies
