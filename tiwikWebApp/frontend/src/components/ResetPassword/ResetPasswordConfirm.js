import React, { useState } from 'react'
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from 'react-bootstrap';
import { useHistory, useParams } from 'react-router';
import axios from "axios";

function ResetPasswordConfirm() {
  const [password, setPassword] = useState('');
  const [passwordShown, setPasswordShown] = useState(false);
  const history = useHistory();
  const { uidb64, token } = useParams();

  const togglePasswordVisiblity = () => {
    setPasswordShown(!passwordShown);
  };

  function handleSubmit() {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const body = {
      new_password1: password,
      new_password2: password,
      uid: uidb64,
      token: token,
    };
    axios.post('/dj-rest-auth/password/reset/confirm/', body, config)
      .then(res => {
        console.log(res)
        history.push("/password/reset/done")
      })
    console.log("Password set")
  }

  return (
    <div className="resetPassword">
      <form>
        <h4 className="resetPassword__header">Set New Password</h4>
        <div className="wrap__password">
          <input
            type={passwordShown ? "text" : "password"}
            className="signup__password"
            placeholder="Password"
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
          <FontAwesomeIcon onClick={togglePasswordVisiblity} className="eyeIcon" icon={passwordShown ? faEye : faEyeSlash} />
        </div>
        <center>
          <Button className="resetPassword__setButton" variant="primary" onClick={handleSubmit}>Confirm Password</Button>
        </center>
      </form>
    </div>
  )
}

export default ResetPasswordConfirm
