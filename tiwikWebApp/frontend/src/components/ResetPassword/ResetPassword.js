import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router';
import "./ResetPassword.css";
import axios from "axios";

function ResetPassword() {
  const [email, setEmail] = useState("");
  const history = useHistory();

  function handleSubmit() {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const body = { email: email };
    axios.post('/dj-rest-auth/password/reset/', body, config)
      .then(res => {
        history.push("/password/reset/sent")
      }).catch(err => {
        console.log(err)
        console.log("error")
      })
  }

  return (
    <div className="resetPassword">
      <p className="resetPassword__info">Enter the email address associated with your account and we will send you instructions to reset your password.</p>
      <form>
        <input
          type="email"
          className="resetPassword__email"
          placeholder="Your Registered Email"
          value={email}
          onChange={e => setEmail(e.target.value)}
        />
      </form>
      <center>
        <Button className="resetPassword__send" variant="primary" onClick={handleSubmit}>Send</Button>
      </center>
    </div>
  )
}

export default ResetPassword
