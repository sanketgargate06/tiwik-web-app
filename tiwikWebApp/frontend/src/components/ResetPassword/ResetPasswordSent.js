import React from 'react'

function ResetPasswordSent() {
  return (
    <div className="resetPasswordSent">
      <p className="resetPassword__info">We have emailed you instructions for setting your password. If an account exists with the email you entered, you will receive them shortly. If you don't receive an email, please make sure you've entered the address you registered with, and check your spam folder.</p>
    </div>
  )
}

export default ResetPasswordSent
