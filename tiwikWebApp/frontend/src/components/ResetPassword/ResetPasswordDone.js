import React from 'react'
import { Link } from 'react-router-dom'

function ResetPasswordDone() {
  return (
    <div className="resetPassword">
      <h5 className="resetPassword__header">Password Reset Successful</h5>
      <center>
        <Link to="/login" className="btn btn-success">Sign In</Link>
      </center>
    </div>
  )
}

export default ResetPasswordDone
