import React, { useState } from "react";
import Modal from "react-modal";
import { useStateValue } from "../../store";
import "./AskAdvice.css";
import axiosInstance from '../../axiosApi';
import Button from 'react-bootstrap/Button'
import { QUESTIONS_LOADED } from "../../constants/actionTypes";

Modal.setAppElement("#root");

function AskAdvice() {
  const [{ user, questions }, dispatch] = useStateValue();
  const [modalIsOpen, setIsOpen] = useState(false);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  const handleSubmit = e => {
    e.preventDefault();
    setIsOpen(false);

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const body = {
      title: title,
      description: description,
      answers: []
    };

    axiosInstance.post('/api/question/', body, config)
      .then(res => {
        questions.unshift(res.data)
        dispatch({
          type: QUESTIONS_LOADED,
          questions: questions,
        })
        setTitle(''),
        setDescription('')
      }).catch(err => {
        console.log(err)
        console.log("error")
      })
  }

  return (
    <>
      {!user ? <></> :
        (<div className="askAdvice">
          <input
            onClick={openModal}
            placeholder={`Ask a question or start a discussion, ${user.first_name}`}
          />
          <Modal
            isOpen={modalIsOpen}
            onRequestClose={closeModal}
            className="askAdvice__Modal"
            contentLabel="Example Modal"
          >
            <form>
              <center>
                <h3 className="askAdvice__header">Ask</h3>
                <input
                  type="textarea"
                  className="askAdvice__title"
                  placeholder={`Ask a question or start a discussion, ${user.first_name}`}
                  value={title}
                  onChange={e => setTitle(e.target.value)}
                />
                <textarea
                  className="askAdvice__description"
                  placeholder="Description...(Optional)"
                  value={description}
                  onChange={e => setDescription(e.target.value)}
                />
                <Button onClick={handleSubmit} variant="primary">Ask</Button>{' '}

                <Button onClick={closeModal} variant="danger">Cancel</Button>{' '}
              </center>
            </form>
          </Modal>
        </div>)}
    </>
  )
}


export default AskAdvice;
