import React, { useState } from "react";
import "./Post.css";
import EditPost from "../EditPost/EditPost";
import PostTitle from "../PostTitle/PostTitle";
import PostAuthor from "../PostAuthor/PostAuthor";
import PostLikes from "../PostLikes/PostLikes";
import DatePublished from "../DatePublished/DatePublished";
import parse from 'html-react-parser';
import axiosInstance from "../../axiosApi";
import { useStateValue } from "../../store";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookmark as faBookmarkRegular } from "@fortawesome/free-regular-svg-icons";
import { faBookmark as faBookmarkSolid } from "@fortawesome/free-solid-svg-icons";
import DeletePost from "../DeletePost/DeletePost";
import { Button, Image } from "react-bootstrap";
import { ARTICLE_UNSAVED, ARTICLE_SAVED, ANSWER_UNSAVED, ANSWER_SAVED } from "../../constants/actionTypes";
import mixpanel from 'mixpanel-browser'
require('dotenv').config()

function Post({ id, author, type, author_slug, slug, title, content, profilePic, published_on, likes, is_saved }) {
  const [{ user, savedPosts, savedAnswers }, dispatch] = useStateValue();
  const [postLikes, setPostLikes] = useState(likes)
  const [readMore, setReadMore] = useState(false);
  const truncatedContent = (content ? (content.split('</p>')[0]) : "")
  const [saved, setSaved] = useState(is_saved);
  mixpanel.init(process.env.MIXPANEL_TOKEN);

  const handleSubmit = (id, type, saved) => {
    if (saved) {
      axiosInstance.delete(`/api/${type}/${id}/like`)
        .then(res => {
          setSaved(res.data.is_saved)
          setPostLikes(res.data.savedCount)
          if (type == "posts") {
            mixpanel.track("Post Unsaved", {
              "Post Title": res.data.postTitle
            });
            const updatedPosts = savedPosts.filter(post => post["id"] != id)
            dispatch({
              type: ARTICLE_UNSAVED,
              savedPosts: updatedPosts
            })
          }
          else if (type == "answers") {
            mixpanel.track("Answer Unsaved", {
              "Answer Title": res.data.title
            });
            const updatedAnswers = savedAnswers.filter(answer => answer["id"] != id)
            dispatch({
              type: ANSWER_UNSAVED,
              savedAnswers: updatedAnswers
            })
          }
        })
    }
    else {
      axiosInstance.post(`/api/${type}/${id}/like`)
        .then(res => {
          setSaved(res.data.is_saved)
          setPostLikes(res.data.savedCount)
          if (type == "posts") {
            mixpanel.track("Post saved", {
              "Post Title": res.data.postTitle
            });
          }
          else if (type == "answers") {
            mixpanel.track("Answer Saved", {
              "Answer Title": res.data.title
            });
            savedAnswers.unshift(res.data)
          }
        })
    }
  }


  return (
    <article className="article__section">
      <div className="media">
        {profilePic ? <Image className="article__author__profilePic" src={profilePic} alt="" />
          : ""
        }
        <div className="media-body">
          <div className="article__metadata">
            <PostTitle title={title} slug={slug} type={type} />
            {author ?
                <PostAuthor author={author} author_slug={author_slug} /> : ""}
            <div className="article__submeta">
              <DatePublished date={published_on} />
              <PostLikes likes={postLikes} />
            </div>
          </div>
        </div>
      </div>
      <div className="answer__section">
        {readMore ? parse(content) : parse(truncatedContent)}
        {content.split('</p>').length > 2 ?
          <span onClick={() => setReadMore(!readMore)} className="answer__readMore">{readMore ? "...Read Less" : "Read More..."}</span>
          : ""}
      </div>
      < div className="replyButton">
        {user.profile.url_slug != author_slug ?
          <Button variant="light" className="likeButton" onClick={() => handleSubmit(id, type, saved)}>
            <FontAwesomeIcon icon={saved ? faBookmarkSolid : faBookmarkRegular} className="likeButtonIcon" />
          </Button>
          : <div className='post__option'>
            <EditPost type={type} qid={id} slug={slug} title={title} />
            <DeletePost type={type} qid={id} slug={slug} />
          </div>
          }
      </div>

    </article>
  );
}

export default Post;

