import React, { useState } from 'react';
import "./Search.css";
import Modal from "react-modal";
import Button from 'react-bootstrap/Button'
import { faSearch, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axiosInstance from "../../axiosApi";
import axios from "axios";
import { Link, useHistory } from "react-router-dom";
import { useStateValue } from '../../store';
import { SEARCH_RESULTS_LOADED } from "../../constants/actionTypes";

function Search() {
  const [{ questionResults, profileResults, postResults }, dispatch] = useStateValue();
  const [modalIsOpen, setIsOpen] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [isSearching, setIsSearching] = useState(false);
  const [display, setDisplay] = useState('false');
  const history = useHistory();

  function openModal() {
    setIsOpen(!modalIsOpen);
  }

  function closeModal() {
    setIsOpen(!modalIsOpen);
  }

  function handleSubmit(e) {
    e.preventDefault();
    axios.all([
      axiosInstance.get(`/api/question/?search=${searchTerm}`),
      axiosInstance.get(`/api/posts/?search=${searchTerm}`),
      axiosInstance.get(`/user/profile/?search=${searchTerm}`)
    ])
      .then(axios.spread((questions, posts, profiles) => {
        dispatch({
          type: SEARCH_RESULTS_LOADED,
          questionResults: questions.data,
          postResults: posts.data,
          profileResults: profiles.data,
          searchTerm: searchTerm
        })
        setSearchTerm('')
        setIsOpen(!modalIsOpen);
        history.push('/results')
      }))
    }

  const updateQuestion = searchValue => {
    setSearchTerm(searchValue);
    setDisplay(!display);
  };

  return (
    <div className="search">
      <button className="search__icon" variant="light" onClick={openModal}>
        <FontAwesomeIcon icon={faSearch} size="lg" />
      </button>

      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        className="search__Modal"
        contentLabel="Example Modal"
      >
        <form onSubmit={handleSubmit}>
          <Button className="search__Modalclose" variant="light" onClick={closeModal}>
            <FontAwesomeIcon icon={faTimes} size='lg' />
          </Button>
          <center>
            <input
              type="textarea"
              placeholder="Search Users, Articles, Discussions..."
              className="search__bar"
              onClick={() => setDisplay(!display)}
              onChange={e => setSearchTerm(e.target.value)}
              value={searchTerm}
            />
            <Button onClick={handleSubmit} className="search__btn" variant="info">Search</Button>
          </center>
        </form>
      </Modal>
    </div >
  )
}

export default Search
