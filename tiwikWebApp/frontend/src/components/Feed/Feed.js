import React, { useState } from "react";
import "./Feed.css";
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Questions from "../Questions/Questions";
import ShareExpertise from "../ShareExpertise/ShareExpertise";
import Articles from "../Articles/Articles";
import Answers from "../Answers/Answers";
import { faQuestionCircle, faBookOpen, faScroll } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Feed() {
  const [loading, setLoading] = useState(false);
  const [key, setKey] = useState('home')

  return (
    <div className="feed">
      <Tabs justify id="controlled-tab-example" activeKey={key} onSelect={(k) => setKey(k)}>
        <Tab eventKey="home" title={<FontAwesomeIcon icon={faBookOpen} className="tabMenuIcon" />}>
          <ShareExpertise />
          <Articles />
        </Tab>
        <Tab eventKey="answers" title={<FontAwesomeIcon icon={faScroll} className="tabMenuIcon" />}>
          <Answers />
        </Tab>
        <Tab eventKey="Ask" title={<FontAwesomeIcon icon={faQuestionCircle} className="tabMenuIcon" />}>
          <Questions />
        </Tab>
      </Tabs>
    </div>
  );
}

export default Feed;
