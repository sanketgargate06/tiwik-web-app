import React from "react";
import "./About.css";

function Cookies() {
	return (
		<div className="about">
			<h2 className="user-legend text-center">Cookie Policy</h2>
			<div className="container">
				<div className="about-div">
					<strong>Effective From:</strong> July 13,2020
					<p className="about-para">
						This Cookie Policy explains how tiwik.in website ("we", "us", and
						"our") uses cookies and similar technologies to recognize you when
						you visit our website at{" "}
						<a href="https://tiwik.in"> https://tiwik.in</a>, ("Website"). It
						explains what these technologies are and why we use them, as well as
						your rights to control our use of them.
					</p>
				</div>
			</div>
		</div>
	);
}

export default Cookies;
