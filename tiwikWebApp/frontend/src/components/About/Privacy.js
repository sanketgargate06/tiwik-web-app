import React from "react";

function Privacy() {
	return (
		<div className="about">
			<legend className="user-legend text-center">Privacy Policy</legend>
			<div className="container text-justify">
				<div className="about-div">
					<strong>Effective From:</strong> July 13,2020
				</div>
			</div>
		</div>
	);
}

export default Privacy;
