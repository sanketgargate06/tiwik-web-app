import React from "react";

function Terms() {
	return (
		<div className="about">
			<legend className="user-legend text-center">Terms of Service</legend>
			<div className="container">
				<div className="about-div">
					<p className="about-para">
						<strong>Effective on:</strong> July 13, 2020{" "}
					</p>
					<p className="about-para">
						<strong>Welcome to Tiwik aka Things I Wish I Knew!</strong>
					</p>
					<h3 className="about-title">Agreement To Terms</h3>
				</div>
			</div>
		</div>
	);
}

export default Terms;
