import React from 'react';
import './About.css';

function About() {
  return (
    <div className="about">
      <legend className="user-legend moto-text text-center">What is Tiwik</legend>
      <div className="container">
        <div className="about-div">
          <p><b>Things I Wish I Knew</b> AKA <b>Tiwik</b> is a platform for people to share their experience and Learn from others experience. Everyone of us face challenges during our lifetime and everyone deals with those challenges in their own way. Depending on the knowledge and resources at one's disposal that person makes a decision about how to tackle those challenges. This sometimes involves re-inventing the wheel.</p>
          <p>Tiwik's mission is to gather these experiences along with Do's and Don'ts, lessons learned from it and how to improve upon them from people who have faced this challenges before and make these experiences available for people who will face those problems in future. We hope that this will help people in making better decisions by minimizing their mistakes and will result in improved productivity.</p>

          <p className="text-center">
            <b> We learn from our mistakes but we don't have to make all of them.</b>
          </p>

        </div>
      </div>    </div>
  )
}

export default About
