import React from 'react'

function Support() {
  return (
    <div className="about">
      <legend className="user-legend moto-text text-center">Contact Us</legend>
      <div className="container text-justify">
        <p className="text-center contact-text">
          <b>Email : </b>support@tiwik.in
        </p>
      </div>
    </div>
  )
}

export default Support
