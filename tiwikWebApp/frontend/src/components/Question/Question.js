import React, { useState } from 'react';
import "./Question.css";
import { Link } from 'react-router-dom';
import { useHistory } from "react-router";
import EditQuestion from '../EditQuestion/EditQuestion';
import EditPost from '../EditPost/EditPost';
import { useStateValue } from '../../store';

function Question({ id, title, description, questionAuthorSlug, slug }) {
  const history = useHistory();
  const [readMore, setReadMore] = useState(false);
  const truncateDescription = description.slice(0, 200);
  const [{ user }, dispatch] = useStateValue();

  return (
    <div className="question">
      <div className="question__wrapper">
        <div className="question__header">
          <Link to={`/discuss/${slug}`} className="question__title">{title}</Link>
          {user ?
            <span className="question__buttons">
              {user.profile.url_slug == questionAuthorSlug ? <>
                <EditQuestion slug={slug} />
              </> : ""}
              <EditPost type="answers" qid={id} slug={slug} title={title} />
            </span> : ""}
        </div>
        {description ?
          <p className="question__description">
            {readMore ? description : truncateDescription}
            {description.length > 200 ?
              <span onClick={() => setReadMore(!readMore)}
                className="answer__readMore">{readMore ? "...Read Less" : " ...Read More"}
              </span>
              : ""}
          </p>
          : ""
        }
      </div>
    </div >
  )
}

export default Question
