import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import "./Settings.css";
import Modal from "react-modal";
import { useHistory, useParams } from 'react-router';
import { ACCOUNT_DELETED } from '../../constants/actionTypes';
import { clear } from "idb-keyval";
import { useStateValue } from '../../store';
import axiosInstance from '../../axiosApi';

function Settings() {
  const [modalIsOpen, setIsOpen] = useState(false);
  const { slug } = useParams();
  const [{ user, profile }, dispatch] = useStateValue();
  const history = useHistory();

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  function DeleteUser() {
    axiosInstance.delete(`/user/profile/${slug}/delete`)
      .then(res => {
        if (res.status == 204) {
          setIsOpen(false);
          localStorage.clear();
          clear()
          dispatch({
            type: ACCOUNT_DELETED,
            user: null,
            profile: null,
          })
          history.push('/');
          console.log('Deleted')
        }
      })
  }

  return (
    <div className="settings">
      <h3>Settings</h3>
      <div className="settingItem">
        <h6>Delete user profile, posts, discussions permanently</h6>
        <Button variant="danger" size="sm" onClick={() => openModal()}>Delete Account</Button>
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={closeModal}
          className="settingItem__Delete"
        >
          <div className="settings_confirmation">
            <h5>This action is irreversible. Please Confirm to delete your data permanently.</h5>
            <div className="settings_confirmationButtons">
              <Button variant="danger" onClick={() => DeleteUser()}>Confirm</Button>
              <Button variant="secondary" onClick={() => closeModal()}>Cancel</Button>
            </div>
          </div>
        </Modal>
      </div>
    </div>
  )
}

export default Settings
