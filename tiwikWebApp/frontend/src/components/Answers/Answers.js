import React, { useEffect } from 'react'
import { useStateValue } from '../../store';
import { ANSWERS_LOADED } from "../../constants/actionTypes";
import Post from '../Post/Post';
import axiosInstance from '../../axiosApi';
import './Answers.css';

function Answers() {
  const [{ answers }, dispatch] = useStateValue();

  const getAnswers = () => {
    axiosInstance.get("/api/answers/")
      .then(res => {
        dispatch({
          type: ANSWERS_LOADED,
          answers: res.data
        })
      })
      .catch((err) => {
        console.log(err)
      });
  }

  useEffect(() => {
    getAnswers();
  }, []);

  return (
    <>
      <h4 className="answers__header">Latest Discussions</h4>
      {
        answers.map((answer) => (
          <Post
            key={answer.id}
            id={answer.id}
            type="answers"
            author_slug={answer.author_slug}
            slug={answer.slug}
            author={answer.author}
            title={answer.title}
            content={answer.content}
            profilePic={answer.profilePic}
            published_on={answer.published_on}
            likes={answer.savedCount}
            is_saved={answer.is_saved}
          />
        )
        )
      }
    </>
  )
}

export default Answers
