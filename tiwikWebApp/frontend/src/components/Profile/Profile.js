import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Post from '../Post/Post';
import { useStateValue } from "../../store";
import './Profile.css';
import Modal from "react-modal";
import axiosInstance from '../../axiosApi';
import { useParams } from "react-router-dom";
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Button from 'react-bootstrap/Button';
import ImageUpload from "../UploadImage/ImageUpload";
import { set, get } from 'idb-keyval';
import Image from 'react-bootstrap/Image'
import Badge from 'react-bootstrap/Badge'
import { APP_LOAD, PROFILE_FOLLOWED, PROFILE_UNFOLLOWED } from "../../constants/actionTypes";
import { Col, Row } from "react-bootstrap";
import { faHandshake, faUserFriends, faBookOpen, faScroll, faCog } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useHistory } from "react-router";
import mixpanel from 'mixpanel-browser'
require('dotenv').config()

Modal.setAppElement("#root");

function Profile() {
  const [{ user, profile }, dispatch] = useStateValue();
  const [userProfile, setUserProfile] = useState({});
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [loading, setLoading] = useState("false");
  const [postsLoading, setPostsLoading] = useState("false");
  const [articles, setArticles] = useState([]);
  const [answers, setAnswers] = useState([]);
  const { slug } = useParams();
  const [image, setimage] = useState();
  const [userBio, setUserBio] = useState();
  const [updatedUserProfiles, setUpdatedUserProfiles] = useState([]);
  const [key, setKey] = useState('home')
  const history = useHistory();
  mixpanel.init(process.env.MIXPANEL_TOKEN);

  function getProfile(slug) {
    setLoading("true");
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    axiosInstance.get(`/user/profile/${slug}`, config)
      .then(res => {
        mixpanel.track("Viewed Profile", {
          "profile": res.data.full_name,
        })
        setUserProfile(res.data);
      })
  }

  function gotoSettings() {
    history.push(`/${slug}/settings`)
  }

  useEffect(() => {
    setLoading(true);
    getProfile(slug);
    setLoading(false);
    async function fetchData() {
      const loggedInProfile = await get('profile')
      const loggedInUser = await get('user')

      dispatch({
        type: APP_LOAD,
        profile: loggedInProfile,
        user: loggedInUser
      })
    }
    fetchData();
  }, []);

  async function getPersonalArticles() {
    try {
      setPostsLoading("true");
      const Posts = await axiosInstance.get(`/api/posts/?search=${slug}`);
      setArticles(Posts.data);
    } catch (error) {
      setPostsLoading("false");
    }
  }

  async function getPersonalAnswers() {
    try {
      setPostsLoading("true");
      const Answers = await axiosInstance.get(`/api/answers/?search=${slug}`);
      setAnswers(Answers.data);
    } catch (error) {
      setPostsLoading("false");
    }
  }

  useEffect(() => {
    getPersonalArticles();
    getPersonalAnswers();
  }, [postsLoading]);

  function openModal() {
    setModalIsOpen(true);
  }

  function closeModal() {
    setModalIsOpen(false);
  }

  const handleSubmit = e => {
    e.preventDefault();
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const body = {
        user_bio: userBio,
    }

    axiosInstance.patch(`/user/profile/${slug}/update`, body, config)
      .then(res => {
        if (res.data) {
          setUserProfile(res.data);
        }
        setModalIsOpen(false);
      })
      .catch((err) => {
        console.error(err)
      });
  }

  function follow({ slug }) {
    axiosInstance.post(`/user/profile/${slug}/follow`)
      .then(res => {
        setUserProfile(res.data)
        mixpanel.track("followed profile", {
          "follower": userProfile.full_name,
          "following": user.profile.full_name
        })
        if (userProfiles.length > 0) {
          setUpdatedUserProfiles(userProfiles.filter(profile => profile["slug"] != slug).unshift(userProfile))
        }
      })
      .catch((err) => {
        console.error(err)
      });
  }

  function unfollow({ slug }) {
    axiosInstance.delete(`/user/profile/${slug}/follow`)
      .then(res => {
        mixpanel.track("unfollowed profile", {
          "follower": userProfile.full_name,
          "following": user.profile.full_name
        })
        setUserProfile(res.data)
        if (userProfiles.length > 0) {
          setUpdatedUserProfiles(userProfiles.filter(profile => profile["slug"] != slug).unshift(userProfile))
        }
      })
  }

  return (
    <div className="profile">
      {user ?
      <div className="profile__card">
          {user.profile.email == userProfile.email ?
            <FontAwesomeIcon icon={faCog} className="profile__settingsIcon" onClick={() => gotoSettings()} /> : ""}
          <span className="profile__imgWrapper">
            <Image className="profile__img" src={userProfile.image} roundedCircle alt="" />
            {user.profile.email == userProfile.email ?
            <ImageUpload profilePic={userProfile.image} slug={slug} onPictureChange={(profile) => setUserProfile(profile)} />
              : ""}
          </span>
        <div className="profile__info">
          <h5 className="profile__info__name">{userProfile.full_name}</h5>
          {user.profile.email == userProfile.email ? <h6 className="profile__info__email">{userProfile.email}</h6> : ''}
          <h6 className="profile__info__userBio">{userProfile.user_bio}</h6>
            <div className="profile__follow__count">
              <div className="profile__followersCount">
                <span>Followers: </span>{userProfile.followers ? userProfile.followers.length : 0}
              </div>
              <div className="profile__followingCount">
                <span>Following: </span>{userProfile.following ? userProfile.following.length : 0}
              </div>
            </div>
          {user.profile.email == userProfile.email ?
              <Badge pill className="profile__edit" onClick={openModal}>Edit Info</Badge> :
              <Badge pill
                className={userProfile.is_following ? "profile__following" : "profile__unfollowing"}
                onClick={userProfile.is_following ? () => unfollow({ slug }) : () => follow({ slug })}>
                {userProfile.is_following ? "Following" : "Follow"}</Badge>
          }
          <Modal
            isOpen={modalIsOpen}
            onRequestClose={closeModal}
            className="profile__edit__Modal"
          >
            <form>
              <center>
                  <h3 className="profile__edit__title">Edit Info</h3>
                <h6>About</h6>
                <textarea
                  type="text"
                  className="profile__edit__userbio"
                  placeholder={userProfile.user_bio ? userProfile.user_bio : "About"}
                  value={userBio}
                  onChange={e => setUserBio(e.target.value)}
                  />
                  <Button onClick={handleSubmit} variant="success">Save</Button>{' '}
                  <Button onClick={closeModal} variant="danger">Cancel</Button>
              </center>
            </form>
          </Modal>
        </div>
        </div > : ""}
        <div className="feed">
        <Tabs justify id="controlled-tab-one" activeKey={key} onSelect={(k) => setKey(k)}>
          <Tab eventKey="home" title={<FontAwesomeIcon icon={faBookOpen} className="tabMenuIcon" />} >
            <center>
              <h5 className="profile__articles__header"> Articles by {userProfile.full_name}</h5>
            </center>
            {articles.map((post) => (
              <Post
                key={post.id}
                id={post.id}
                type="posts"
                author_slug={post.author_slug}
                slug={post.slug}
                title={post.postTitle}
                content={post.postContent}
                published_on={post.published_on}
                likes={post.savedCount}
                is_saved={post.is_saved}
              />
            ))}
          </Tab>
          <Tab eventKey="answers" title={<FontAwesomeIcon icon={faScroll} className="tabMenuIcon" />}>
            <center>
              <h5 className="answers__header"> Discussions by {userProfile.full_name}</h5>
            </center>
            {answers.map((post) => (
              <Post
                key={post.id}
                id={post.id}
                type="answers"
                author_slug={post.author_slug}
                slug={post.slug}
                title={post.title}
                content={post.content}
                published_on={post.published_on}
                likes={post.savedCount}
                is_saved={post.is_saved}
              />
            ))}
          </Tab>
          <Tab eventKey="following" title={<FontAwesomeIcon icon={faHandshake} className="tabMenuIcon" />}>
            <div className="profile__followWrapper">
              <h5 className="profile__followHeader">Following</h5>
              {userProfile.following ?
                <Row>
                  {userProfile.following.map((following) => (
                    <Col md={4}>
                      <div key={following.url_slug} className="profile__followItem">
                        <Image className="profile__followImg" src={following.image} alt="" roundedCircle />
                        <Link to={`/ profile / ${following.url_slug}`} target="_blank" className="profile__followItemName">{following.full_name}</Link>
                      </div>
                    </Col>
                  ))}
                </Row> :
                <p>You are not following anybody</p>
              }
            </div>
          </Tab>
          <Tab eventKey="followers" title={<FontAwesomeIcon icon={faUserFriends} className="tabMenuIcon" />}>
            <div className="profile__followWrapper">
              <h5 className="profile__followHeader">Followers</h5>
              {userProfile.followers ?
                <Row>
                  {userProfile.followers.map((follower) => (
                    <Col md={4}>
                      <div key={follower.url_slug} className="profile__followItem">
                        <Image className="profile__followImg" src={follower.image} alt="" roundedCircle />
                        <Link to={`/ profile / ${follower.url_slug}`} target="_blank" className="profile__followItemName">{follower.full_name}</Link>
                      </div>
                    </Col>
                  ))}
                </Row> :
                <center>
                  <h5>
                    No Followers to Show
                  </h5>
                </center>
              }
            </div>
          </Tab>
        </Tabs>
      </div>
    </div>
  );
}

export default Profile;
