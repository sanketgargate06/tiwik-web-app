import React, { useState } from 'react'
import ImageCropper from './ImageCropper'
import { faCameraRetro } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Button from 'react-bootstrap/Button';
import Modal from "react-modal";
import { useStateValue } from "../../store";
import axiosInstance from '../../axiosApi';
import './ImageUpload.css';
import { PROFILE_UPDATED } from "../../constants/actionTypes";
import { set } from "idb-keyval";

function ImageUpload({ profilePic, slug, onPictureChange }) {
  const [{ user, profile }, dispatch] = useStateValue();
  const [blob, setBlob] = useState(null)
  const [inputImg, setInputImg] = useState('')
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [uploading, setUploading] = useState(false);

  const getBlob = (blob) => {
    setBlob(blob)
  }

  const onInputChange = (e) => {
    const file = e.target.files[0]
    const reader = new FileReader()

    reader.addEventListener('load', () => {
      setInputImg(reader.result)
    }, false)

    if (file) {
      reader.readAsDataURL(file)
    }
  }

  const handleSubmitImage = (e) => {
    e.preventDefault()
    setUploading(true)
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    };

    let form_data = new FormData();
    form_data.append('image', blob)
    axiosInstance.patch(`/user/profile/${slug}/update`, form_data, config)
      .then(res => {
        set("profile", res.data)
        dispatch({
          type: PROFILE_UPDATED,
          profile: res.data,
        })
        onPictureChange(res.data)
        setUploading(false)
        setModalIsOpen(false)
      }).catch(err => console.log(err))
  };

  return (
    <span className="uploadImage" >
      <Button variant="light" className="uploadImage__edit" onClick={() => setModalIsOpen(true)}>
        <FontAwesomeIcon icon={faCameraRetro} />
      </Button>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={() => setModalIsOpen(false)}
        className="profileImage__edit__Modal"
      >
        {uploading ? <div className="loader simple-circle"></div>
          :
          <form onSubmit={handleSubmitImage}>

            <div className="upload__inputImage">
              {
                inputImg ?
                  <ImageCropper
                    getBlob={getBlob}
                    inputImg={inputImg}
                  /> :
                  <ImageCropper
                    getBlob={getBlob}
                    inputImg={profilePic}
                  />
              }
            </div>
            <div className="text-center">
              <div className="upload__input">
                Choose Image
              <input
                  type='file'
                  accept='image/*'
                  onChange={onInputChange}
                />
              </div>
              <div className="upload-buttons">
                <Button type="submit" variant="success">Save</Button>
                <Button onClick={() => setModalIsOpen(false)} variant="danger">Cancel</Button>
              </div>
            </div>
          </form>
        }
      </Modal >

    </span >
  )
}

export default ImageUpload