import React from "react";

function PostLikes({ likes }) {
  return (
    <div className="article__likes">
      {likes} {likes == 1 ? "bookmark" : "bookmarks"}
    </div>
  );
}

export default PostLikes;
