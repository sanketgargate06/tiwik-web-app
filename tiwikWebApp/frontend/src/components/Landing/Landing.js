import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import Footer from '../Footer/Footer';
import './Landing.css';

function Landing() {
  return (
    <Fragment>
      <div className="landing">
        <div className="landing__slideOne">
          <div className="landing__IllustrationOne">
            <img className="landing__picOne" src="https://dyvdhh93lcjbp.cloudfront.net/Reading.gif" alt="" />
          </div>
          <div className="landing__tagOne">
            <div className="landing__tagline">
              <span>Everyone has a story, tell yours!</span>
              <p>Share stories of your adventures, challenges, projects</p>
            </div>
            <Link to="/register" className="landing__Join">Start Sharing - For Free</Link>
          </div>
        </div>
        <div className="landing__slideTwo">
          <div className="landing__tagTwo">
            <div className="landing__tagline">
              <span>Ask Tiwik</span>
              <p>Starting Something New ? Don't know where to start ? Ask for help</p>
            </div>
            <Link to="/register" className="landing__Join">Join Now - It's free</Link>
          </div>
          <div className="landing__IllustrationTwo">
            <img className="landing__picTwo" src="https://dyvdhh93lcjbp.cloudfront.net/ProblemSolving.gif" alt="" />
          </div>
        </div>
        <div className="landing__slideOne">
          <div className="landing__IllustrationOne">
            <img className="landing__picThree" src="https://dyvdhh93lcjbp.cloudfront.net/Notebook.gif" alt="" />
          </div>
          <div className="landing__tagThree">
            <div className="landing__tagline">
              <span>Share Your Experiences</span>
              <p>Share your experiences in Travel, Startup, Projects & Many More.</p>
            </div>
            <Link to="/register" className="landing__Join">Start Sharing- It's free</Link>
          </div>
        </div>
      </div>
      <Footer />
    </Fragment>
  )
}

export default Landing 
