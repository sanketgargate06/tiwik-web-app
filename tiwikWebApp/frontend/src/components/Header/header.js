import React, { useState } from "react";
import "./header.css";
import { NavLink, useHistory } from "react-router-dom";
import { faHome, faUser, faSignOutAlt, faBookmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useStateValue } from "../../store";
import { LOGOUT_SUCCESS } from "../../constants/actionTypes";
import { Container, Nav, Navbar, NavItem } from "react-bootstrap";
import Search from "../Search/Search";
import { clear } from "idb-keyval";


function Header() {
  const history = useHistory();
  const [{ user, profile }, dispatch] = useStateValue();
  const [expanded, setExpanded] = useState(false);

  const logout = () => {
    localStorage.clear();
    setExpanded(false);
    clear()
    dispatch({
      type: LOGOUT_SUCCESS,
      user: null,
      profile: null,
    })
    history.push('/');
  }


  return (
    <header className="site-header">
      { profile ? (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" fixed="top" className="header logged-in" expanded={expanded}>
            <Container>
            <NavLink to="/" className="site__logoWrapper">
              <img src="https://dyvdhh93lcjbp.cloudfront.net/logo_White_Shadow.svg" className="site__logo" />
              </NavLink>
            <div className="header__menu">
            <div className="header__search">
              <Search />
            </div>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" onClick={() => setExpanded(expanded ? false : "expanded")}>
              {profile.image ?
                <img
                  className="header__img"
                  src={profile.image}
                /> : ""}
              </Navbar.Toggle>
            </div>
              <Navbar.Collapse id="navbarSupportedContent">
              <Nav>
                  <Nav.Item>
                  <NavLink className="nav-link" to="/" onClick={() => setExpanded(false)}>
                      <FontAwesomeIcon icon={faHome} />
                      <span>Home</span>
                    </NavLink>
                  </Nav.Item>
                  <Nav.Item>
                  <NavLink className="nav-link" to={`/profile/${user.url_slug}`} onClick={() => setExpanded(false)}>
                      <FontAwesomeIcon icon={faUser} />
                    <span>{user ? user.profile.full_name : ""}</span>
                    </NavLink>
                  </Nav.Item>
                  <Nav.Item>
                  <NavLink className="nav-link" to={`/${user.url_slug}/saved`} onClick={() => setExpanded(false)}>
                    <FontAwesomeIcon icon={faBookmark} />
                    <span>Saved</span>
                  </NavLink>
                </Nav.Item>
                <Nav.Item>
                    <NavLink className="nav-link" to="/" onClick={logout}>
                      <FontAwesomeIcon icon={faSignOutAlt} />
                      <span>Logout</span>
                    </NavLink>
                  </Nav.Item>
                </Nav>
              </Navbar.Collapse>
            </Container>
          </Navbar>
        ) : (
            <Navbar bg="dark" variant="dark" fixed="top" className="header logged-out">
              <Container>
              <NavLink to="/" className="site__logoWrapper">
                <img src="https://dyvdhh93lcjbp.cloudfront.net/logo_White_Shadow.svg" className="site__logo" />
              </NavLink>
                <Nav className="visitor">
                  <NavItem>
                    <NavLink className="nav-link" to="/login">
                      <span>Sign In</span>
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink className="nav-link signup-link" to="/register">
                      <span>Sign Up</span>
                    </NavLink>
                  </NavItem>
                </Nav>
              </Container>
            </Navbar>
          )
      }
    </header>
  );
}

export default Header;
