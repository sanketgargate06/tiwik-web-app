import axios from 'axios'
require('dotenv').config()

const axiosInstance = axios.create({
  baseURL: process.env.BASE_URL,
  timeout: 5000,
  headers: {
    'Authorization': "JWT " + JSON.parse(localStorage.getItem('access_token')),
    'Content-Type': 'application/json',
    'accept': 'application/json'
  }
});

axiosInstance.interceptors.response.use(
  response => response,
  error => {
    const originalRequest = error.config;

    if (error.response.status === 401 && error.response.statusText === "Unauthorized") {
      const refresh_token = JSON.parse(localStorage.getItem('refresh_token'));

      return axiosInstance
        .post('/api/token/refresh/', { refresh: refresh_token })
        .then((response) => {
          localStorage.setItem('access_token', JSON.stringify(response.data.access));
          localStorage.setItem('refresh_token', JSON.stringify(response.data.refresh));

          axiosInstance.defaults.headers['Authorization'] = "JWT " + response.data.access;
          originalRequest.headers['Authorization'] = "JWT " + response.data.access;

          return axiosInstance(originalRequest);
        })
        .catch(err => { 
          console.log(err)
        });
    }
    return Promise.reject(error);
  }
);

export default axiosInstance;