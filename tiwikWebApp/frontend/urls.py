from django.urls import path, re_path
from . import views
from django.views.generic.base import TemplateView

urlpatterns = [
    path('', views.index),
    re_path(r'^(?:.*)/?$',  views.index),
]
