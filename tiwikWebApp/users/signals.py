from django.db.models.signals import post_save
from django.contrib.auth import get_user_model
from django.dispatch import receiver
from .models import Profile
from axes.signals import user_locked_out
from rest_framework.exceptions import PermissionDenied

User = get_user_model()

@receiver(post_save,sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.get_or_create(user=instance)

# @receiver(post_save, sender=User)
# def create_profile_for_user(sender, instance=None, created=False, **kwargs):
#     if created:
#         Profile.objects.get_or_create(user=instance)


@receiver(user_locked_out)
def raise_permission_denied(*args, **kwargs):
    raise PermissionDenied("Too many failed login attempts")


@receiver(post_save,sender=User)
def save_profile(sender, instance, **kwargs):
    instance.profile.save()
