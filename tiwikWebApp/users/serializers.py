from django.contrib.auth import get_user_model, authenticate
from .models import Profile, Contact
from django.conf import settings
from rest_framework import serializers, exceptions
from dj_rest_auth.serializers import UserDetailsSerializer
try:
    from allauth.account import app_settings as allauth_settings
    from allauth.utils import email_address_exists
    from allauth.account.adapter import get_adapter
    from allauth.account.utils import setup_user_email
    from allauth.socialaccount.helpers import complete_social_login
    from allauth.socialaccount.models import SocialAccount
    from allauth.socialaccount.providers.base import AuthProcess
except ImportError:
    raise ImportError("allauth needs to be added to INSTALLED_APPS.")
from django.db import transaction
from django.shortcuts import get_object_or_404
from posts.serializers import PostSaveSerializer, AnswerSaveSerializer

User = get_user_model()


class CustomLoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=False, allow_blank=True)
    password = serializers.CharField(style={'input_type': 'password'})

    def authenticate(self, **kwargs):
        return authenticate(self.context['request'], **kwargs)

    def _validate_email(self, email, password):
        user = None

        if email and password:
            user = self.authenticate(email=email, password=password)
        else:
            msg = _('Must include "email" and "password".')
            raise exceptions.ValidationError(msg)

        return user

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        user = None

        if 'allauth' in settings.INSTALLED_APPS:
            from allauth.account import app_settings

            # Authentication through email
            if app_settings.AUTHENTICATION_METHOD == app_settings.AuthenticationMethod.EMAIL:
                user = self._validate_email(email, password)

        else:
            # Authentication without using allauth
            if email:
                user = self._validate_email(email, password)

        # Did we get back an active user?
        if user:
            if not user.is_active:
                msg = _('User account is disabled.')
                raise exceptions.ValidationError(msg)
        else:
            msg = _('Unable to log in with provided credentials.')
            raise exceptions.ValidationError(msg)

        attrs['user'] = user
        return attrs


class Base64ImageField(serializers.ImageField):

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            # 12 characters are more than enough.
            file_name = str(uuid.uuid4())[:12]
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension, )

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension


class FollowingSerializer(serializers.ModelSerializer):
    following_data = serializers.SerializerMethodField()

    class Meta:
        model = Contact
        fields = ['following_data', ]

    def get_following_data(self, obj):
        return ProfileSerializer(obj.user_to).data


class FollowersSerializer(serializers.ModelSerializer):
    followers_data = serializers.SerializerMethodField()

    class Meta:
        model = Contact
        fields = ["followers_data"]

    def get_followers_data(self, obj):
        return ProfileSerializer(obj.user_from).data


class ProfileSerializer(serializers.ModelSerializer):
    image = Base64ImageField(max_length=None, required=False, use_url=True,)
    user_bio = serializers.CharField(
        max_length=255, trim_whitespace=True, required=False,)
    full_name = serializers.ReadOnlyField(source='user.full_name')
    email = serializers.ReadOnlyField(source='user.email')
    url_slug = serializers.ReadOnlyField(source='user.url_slug')
    is_following = serializers.SerializerMethodField()
    following = serializers.SerializerMethodField()
    followers = serializers.SerializerMethodField()
    savedAnswers = serializers.SerializerMethodField()
    savedPosts = serializers.SerializerMethodField()

    class Meta:
        model = Profile
        fields = ['image', 'user_bio', 'full_name',  'following', 'followers',

                  'is_following', 'email', 'url_slug', 'savedAnswers', 'savedPosts']

    def get_following(self, instance):
        request = self.context.get('request', None)

        if request is None:
            return False

        following_list = []
        for _ in FollowingSerializer(instance.followed.all(), many=True).data:
            following_list.append(list(_.values()))
        following = [item for sublist in following_list for item in sublist]
        return following

    def get_followers(self, instance):
        request = self.context.get('request', None)

        if request is None:
            return False

        followers_list = []
        for _ in FollowersSerializer(instance.following.all(), many=True).data:
            followers_list.append(list(_.values()))
        followers = [item for sublist in followers_list for item in sublist]
        return followers

    def get_is_following(self, instance):
        request = self.context.get('request', None)

        if request is None:
            return False

        if not request.user.is_authenticated:
            return False

        profile = request.user.profile
        following_list = []
        is_following = False
        for _ in FollowingSerializer(profile.followed.all(), many=True).data:
            following_list.append(list(_.values()))
        following = [item for sublist in following_list for item in sublist]

        for _ in following:
            if _["url_slug"] == instance.user.slug:
                is_following = True
        return is_following

    def get_savedAnswers(self, instance):
        request = self.context.get('request', None)

        if request is None:
            return False

        if not request.user.is_authenticated:
            return False

        nested_saved_answers = []
        for _ in AnswerSaveSerializer(instance.userSavingAnswer.all(), many=True, context={ "request": request}).data:
            nested_saved_answers.append(_.values())
        saved_answers = [
            item for sublist in nested_saved_answers for item in sublist]
        return saved_answers

    def get_savedPosts(self, instance):
        request = self.context.get('request', None)

        if request is None:
            return False

        if not request.user.is_authenticated:
            return False

        nested_saved_posts = []
        for _ in PostSaveSerializer(instance.userSavingPost.all(), many=True, context={ "request": request}).data:
            nested_saved_posts.append(_.values())
        saved_posts = [
            item for sublist in nested_saved_posts for item in sublist]
        return saved_posts


class CustomUserDetailsSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'url_slug', 'profile']

        read_only_fields = ['email', ]

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('profile', {})
        image = profile_data.get('image')
        user_bio = profile_data.get('user_bio')

        instance = super(CustomUserDetailsSerializer,
                         self).update(instance, validated_data)

        # get and update user profile
        profile = instance.profile
        if profile_data:
            profile.image = image
            profile.user_bio = user_bio
            profile.save()
        return instance


class CustomRegisterSerializer(serializers.Serializer):
    email = serializers.EmailField(required=allauth_settings.EMAIL_REQUIRED)
    first_name = serializers.CharField(required=True, write_only=True)
    last_name = serializers.CharField(required=True, write_only=True)
    password1 = serializers.CharField(required=True, write_only=True)
    password2 = serializers.CharField(required=True, write_only=True)

    def validate_email(self, email):
        email = get_adapter().clean_email(email)
        if allauth_settings.UNIQUE_EMAIL:
            if email and email_address_exists(email):
                raise serializers.ValidationError(
                    _("A user is already registered with this e-mail address."))
        return email

    def validate_password1(self, password):
        return get_adapter().clean_password(password)

    def validate(self, data):
        if data['password1'] != data['password2']:
            raise serializers.ValidationError(
                _("The two password fields didn't match."))
        return data

    def get_cleaned_data(self):
        return {
            'first_name': self.validated_data.get('first_name', ''),
            'last_name': self.validated_data.get('last_name', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
        }

    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        self.cleaned_data = self.get_cleaned_data()
        adapter.save_user(request, user, self)
        setup_user_email(request, user, [])
        user.save()
        return user
