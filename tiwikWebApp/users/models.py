from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin, BaseUserManager,
)
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.template.defaultfilters import slugify
from stdimage import JPEGField
from datetime import date
import uuid


class UserManager(BaseUserManager):

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """PermissionsMixin contains the following fields:
        - `is_superuser`
        - `groups`
        - `user_permissions`
     You can omit this mix-in if you don't want to use permissions or
     if you want to implement your own permissions logic.
     """

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")
        db_table = 'auth_user'
        # `db_table` is only needed if you move from the existing default
        # User model to a custom one. This enables to keep the existing data.

    USERNAME_FIELD = 'email'
    """Use the email as unique username."""

    REQUIRED_FIELDS = ['first_name', 'last_name']

    email = models.EmailField(
        verbose_name=_("email address"), unique=True,
        error_messages={
            'unique': _(
                "A user is already registered with this email address"),
        },
    )

    first_name = models.CharField(
        max_length=30, verbose_name=_("first name"),
    )
    last_name = models.CharField(
        max_length=30, verbose_name=_("last name"),
    )

    is_staff = models.BooleanField(
        verbose_name=_("staff status"),
        default=False,
        help_text=_(
            "Designates whether the user can log into this admin site."
        ),
    )

    slug = models.CharField(
        max_length=80, verbose_name=_("slug"), default='Null'
    )

    is_active = models.BooleanField(
        verbose_name=_("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )

    date_joined = models.DateTimeField(
        verbose_name=_("date joined"), default=timezone.now,
    )

    def save(self, *args, **kwargs):
        full_name = self.first_name + ' ' + self.last_name
        basicSlug = slugify(full_name)
        sameNameUsers = User.objects.filter(
            first_name=self.first_name, last_name=self.last_name).order_by('pk')
        self.slug = basicSlug + '-' + str(len(sameNameUsers))
        super(User, self).save(*args, **kwargs)

    @property
    def full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    @property
    def url_slug(self):
        return ('{}'.format(self.slug))

    objects = UserManager()

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True


class Profile(models.Model):
    def user_directory_path(self, filename):
        today = date.today()
        d1 = today.strftime("%d-%m-%Y")
        return "{0}/{1}-{2}".format(self.user.url_slug, 'photo', d1)

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    user_bio = models.CharField(
        max_length=255, verbose_name=_("user bio"), blank=True, null=True,
    )
    image = JPEGField(default='default.jpeg', upload_to=user_directory_path,
                      variations={'thumbnail': (300, 300)})
    birth_date = models.DateField(null=True, blank=True)
    signup_confirmation = models.BooleanField(default=True)

    @property
    def get_image_url(self):
        if self.image.thumbnail and hasattr(self.image.thumbnail, 'url'):
            return self.image.thumbnail.url
        else:
            return 'https://dyvdhh93lcjbp.cloudfront.net/default.jpeg'

    def __str__(self):
        return f'{self.user.full_name}'


class Contact(models.Model):

    user_from = models.ForeignKey(
        Profile, related_name="followed", on_delete=models.CASCADE)
    user_to = models.ForeignKey(
        Profile, related_name="following", on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['user_from', 'user_to'],  name="unique_followers")
        ]

        ordering = ["-created"]


Profile.add_to_class('follows', models.ManyToManyField(

    'self', through=Contact, related_name='followers', symmetrical=False))
