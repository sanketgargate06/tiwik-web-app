from django.contrib import admin
from django.contrib.auth import get_user_model
from .models import Profile

User = get_user_model()
admin.site.site_header = 'Tiwik Admin Dashboard'

class AccountAdmin(admin.ModelAdmin):
    list_display = ('email','first_name','last_name','date_joined', 'is_staff')
    list_filter = ('date_joined',)
    search_fields = ('email','first_name','last_name')
    readonly_fields = ('date_joined',)
    ordering = ('date_joined','last_name')
    

admin.site.register(User, AccountAdmin)
admin.site.register(Profile)
