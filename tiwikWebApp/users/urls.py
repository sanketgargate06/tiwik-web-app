from django.urls import path, include
from .views import(
    GoogleLogin,
    FacebookLogin,
    ProfileRetrieveAPIView,
    ProfileFollowAPIView,
    ProfileUpdateView,
    ProfileViewSet,
    DeleteUserView)
from rest_framework import routers

router = routers.DefaultRouter()
router.register('profile', ProfileViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('auth/google/', GoogleLogin.as_view(), name='google_login'),
    path('profile/<slug:slug>', ProfileRetrieveAPIView.as_view()),
    path('profile/<slug:slug>/update', ProfileUpdateView.as_view()),
    path('profile/<slug:slug>/follow', ProfileFollowAPIView.as_view()),
    path('profile/<slug:slug>/delete', DeleteUserView.as_view())
]
