from django.shortcuts import render
from rest_framework import serializers, status, viewsets
from rest_framework.exceptions import NotFound, ParseError
from .serializers import ProfileSerializer, FollowingSerializer, CustomUserDetailsSerializer
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from dj_rest_auth.registration.views import SocialLoginView
from rest_framework.generics import RetrieveAPIView, UpdateAPIView
from django.contrib.auth import get_user_model
from .models import Profile, Contact
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.parsers import FileUploadParser
from rest_framework import filters
from rest_framework.request import Request
from django.http import Http404

User = get_user_model()


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter


class ProfileRetrieveAPIView(RetrieveAPIView):
    permission_classes = [IsAuthenticated, ]
    queryset = Profile.objects.select_related('user').order_by('user__pk')
    serializer_class = ProfileSerializer
    parser_class = (FileUploadParser,)

    def retrieve(self, request, slug, *args, **kwargs):
        try:
            profile = self.queryset.get(user__slug=slug)
        except Profile.DoesNotExist:
            raise NotFound('A profile with this username does not exist.')

        serializer = self.serializer_class(
            profile, context={'request': request})

        return Response(serializer.data, status=status.HTTP_200_OK)

    def get_serializer_context(self):
        context = super(ProfileRetrieveAPIView, self).get_serializer_context()
        context.update({"request": self.request})
        return context

class ProfileUpdateView(UpdateAPIView):
    permission_classes = [IsAuthenticated, ]
    queryset = Profile.objects.select_related('user').order_by('user__pk')
    serializer_class = ProfileSerializer
    parser_class = (FileUploadParser,)

    def get_serializer_context(self):
        context = super(ProfileUpdateView, self).get_serializer_context()
        context.update({"request": self.request})
        return context


    def patch(self, request, slug):
        try:
            profile = self.queryset.get(user__slug=slug)
            if profile.user == request.user:
                if 'image' in request.data and request.data['image'] != 'null':
                    profile.image = request.data['image']
                elif 'user_bio' in request.data and request.data['user_bio'] != 'null':
                    profile.user_bio = request.data['user_bio']
                profile.save()
                serializer = self.serializer_class(profile, context={
                    'request': request
                })
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response({'Forbidden': 'You are not authorized'}, status=status.HTTP_403_FORBIDDEN)
        except Profile.DoesNotExist:
            raise NotFound('A profile with this username does not exist.')


class ProfileFollowAPIView(APIView):
    permission_classes = [IsAuthenticated, ]
    serializer_class = ProfileSerializer

    def get_serializer_context(self):
        context = super(ProfileFollowAPIView, self).get_serializer_context()
        context.update({"request": self.request})
        return context


    def delete(self, request, slug=None):
        follower = self.request.user.profile
        try:
            followee = Profile.objects.get(user__slug=slug)
        except Profile.DoesNotExist:
            raise NotFound('A profile with this username was not found.')

        try:
            Contact.objects.filter(

                user_from=follower, user_to=followee).delete()
            print('*********************************************')
            print("{} Unfollowed {}".format(

                follower.user.full_name,  followee.user.full_name))

        except Contact.DoesNotExist:
            raise NotFound("A Contact Not Found")

        serializer = self.serializer_class(followee, context={
            'request': request
        })
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, slug=None):
        follower = self.request.user.profile
        try:
            followee = Profile.objects.get(user__slug=slug)

        except Profile.DoesNotExist:
            raise NotFound('A profile with this username was not found.')

        if follower.pk is followee.pk:
            raise serializers.ValidationError('You can not follow yourself.')

        try:
            Contact.objects.create(user_from=follower, user_to=followee)
            print('*********************************************')
            print("{} Followed {}".format(

                follower.user.full_name,  followee.user.full_name))
        except Contact.DoesNotExist:
            raise NotFound("A Contact Not Found")

        serializer = self.serializer_class(followee, context={
            'request': request
        })

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ProfileViewSet(viewsets.ModelViewSet):
    search_fields = ['user__slug', ]
    filter_backends = (filters.SearchFilter,)
    permission_classes = [IsAuthenticated, ]
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    def get_serializer_context(self):
        context = super(ProfileViewSet, self).get_serializer_context()
        context.update({"request": self.request, "user": self.request.user})
        return context


class DeleteUserView(APIView):

    def delete(self, request, *args, **kwargs):
        user = User.objects.get(slug=kwargs['slug'])
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
