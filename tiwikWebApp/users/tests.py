from django.test import TestCase

from .models import Profile
from django.contrib.auth import get_user_model

User = get_user_model()

class ProfileTestCase(TestCase):
    def setUp(self):
        self.user_a = User.objects.create_user(
            email='test@test.com',
            first_name = 'Test',
            last_name = 'User',
            password='password')
        self.user_b = User.objects.create_user(
            email='test1@test.com',
            first_name = 'Test1',
            last_name = 'User',
            password='password')

    def test_profile_created_via_signal(self):
        qs = Profile.objects.all()
        self.assertEqual(qs.count(),2)

    def test_following(self):
        first = self.user_a
        second = self.user_b
        first.profile.followers.add(second)
        second_user_following_whom = second.following.all()
        qs = second_user_following_whom.filter(user=first)
        self.assertTrue(qs.exists())
        first_user_following_no_one = first.following.all()
        self.assertFalse(first_user_following_no_one.exists())