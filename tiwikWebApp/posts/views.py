from rest_framework import viewsets, serializers, permissions, renderers, authentication
from rest_framework.decorators import action
from .serializers import QuestionSerializer, AnswerSerializer, PostSerializer, AnswerSaveSerializer, PostSaveSerializer, QuestionSaveSerializer
from rest_framework.response import Response
from .models import Question, Answer, Post, AnswerSave, QuestionSave, PostSave
from users.models import Profile
from .permissions import IsOwnerOrReadonly
from rest_framework import filters
from rest_framework.views import APIView
from django.contrib.auth import get_user_model
from rest_framework_simplejwt.authentication import JWTAuthentication
from django.views.decorators.csrf import csrf_exempt
from rest_framework.generics import RetrieveAPIView, UpdateAPIView, ListAPIView
from rest_framework.exceptions import NotFound, ParseError
from rest_framework import status

User = get_user_model()


class AnswerViewSet(viewsets.ModelViewSet):
    search_fields = ['author__slug']
    filter_backends = (filters.SearchFilter,)
    queryset = Answer.objects.all().order_by('-published_on')
    serializer_class = AnswerSerializer

    permission_classes = [
        permissions.IsAuthenticated, IsOwnerOrReadonly]

    def get_serializer_context(self):
        context = super(AnswerViewSet, self).get_serializer_context()
        context.update({"request": self.request})
        return context

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class PostViewSet(viewsets.ModelViewSet):
    search_fields = ['postTitle', 'author__slug']
    filter_backends = (filters.SearchFilter,)
    queryset = Post.objects.all().order_by('-postPublished')
    serializer_class = PostSerializer
    permission_classes = [
        permissions.IsAuthenticated,    IsOwnerOrReadonly]

    def get_serializer_context(self):
        context = super(PostViewSet, self).get_serializer_context()
        context.update({"request": self.request})
        return context

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class QuestionViewSet(viewsets.ModelViewSet):
    search_fields = ['title', ]
    filter_backends = (filters.SearchFilter,)
    queryset = Question.objects.all().order_by('-created_on')
    serializer_class = QuestionSerializer
    permission_classes = [
        permissions.IsAuthenticated, ]
    lookup_field = 'slug'

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    def get_serializer_context(self):
        context = super(QuestionViewSet, self).get_serializer_context()
        context.update({"request": self.request})
        return context


class AnswerLikeAPIToggle(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None, pk=None):
        user = self.request.user
        answer = Answer.objects.get(pk=pk)
        author = answer.author
        updated = False
        liked = False
        if author.id != user.id:
            if user in answer.likes.all():
                liked = False
                answer.likes.remove(user)
            else:
                liked = True
                answer.likes.add(user)
            updated = True
        else:
            return Response({'You cannot like your own answer'})
        data = {
            "updated": updated,
            "liked": liked,
            "likes": answer.likes.all().values_list('pk', flat=True)
        }
        return Response(data)


class AnswerSaveAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = AnswerSerializer

    def delete(self, request, pk=None):
        profile = self.request.user.profile
        try:
            answer = Answer.objects.get(id=pk)
        except Answer.DoesNotExist:
            raise NotFound('A profile with this username was not found.')

        try:
            AnswerSave.objects.filter(
                profile=profile, answer=answer).delete()
        except AnswerSave.DoesNotExist:
            raise NotFound("Saved Answer Not Found")

        serializer = self.serializer_class(answer, context={
            'request': request
        })
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, pk=None):
        profile = self.request.user.profile

        try:
            answer = Answer.objects.get(id=pk)
            if answer.author.slug == profile.user.slug:
                raise NotImplementedError()

        except Answer.DoesNotExist:
            raise NotFound('A answer with this id was not found.')

        try:
            AnswerSave.objects.create(profile=profile, answer=answer)

        except AnswerSave.DoesNotExist:
            raise NotFound("Saved Answer Not Found")

        serializer = self.serializer_class(answer, context={
            'request': request
        })

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class PostSaveAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = PostSerializer

    def delete(self, request, pk=None):
        profile = self.request.user.profile
        try:
            post = Post.objects.get(id=pk)
        except Post.DoesNotExist:
            raise NotFound('A profile with this username was not found.')

        try:
            PostSave.objects.filter(
                profile=profile, post=post).delete()

        except PostSave.DoesNotExist:
            raise NotFound("Saved Post Not Found")

        serializer = self.serializer_class(post, context={
            'request': request
        })
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, pk=None):
        profile = self.request.user.profile

        try:
            post = Post.objects.get(id=pk)
            if post.author.slug == profile.user.slug:
                raise NotImplementedError()

        except Answer.DoesNotExist:
            raise NotFound('A post with this id was not found.')

        try:
            PostSave.objects.create(profile=profile, post=post)

        except PostSave.DoesNotExist:
            raise NotFound("A Saved Answer Not Found")

        serializer = self.serializer_class(post, context={
            'request': request
        })

        return Response(serializer.data, status=status.HTTP_201_CREATED)
