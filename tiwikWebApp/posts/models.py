from django.db import models
from django.template.defaultfilters import slugify
from django.conf import settings
from django.utils import timezone
from django.urls import reverse
from tinymce import HTMLField
from django.conf import settings
from users.models import Profile


class Question(models.Model):
    title = models.CharField(unique=True, max_length=255)
    description = models.TextField('Description', blank=True)
    slug = models.SlugField(unique=True, max_length=255, editable=False)
    created_on = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Question, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("post-detail", kwargs={"slug": self.slug})


class Answer(models.Model):
    content = models.TextField('Content', blank=True)
    published_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    question = models.ForeignKey(
        Question, on_delete=models.PROTECT, related_name='answers')

    def __str__(self):
        return self.question.title

    # def get_api_like_url(self):
    #     return reverse("answer-like-api", kwargs={"slug": self.question.slug, "author": self.author.id})

    @property
    def custom_class(self):
        return '{}-{}'.format(self.question.id, self.author.id)

    class Meta:
        unique_together = ('author', 'question',)


class Post(models.Model):
    postTitle = models.CharField(unique=True, max_length=255)
    postContent = HTMLField('Content')
    slug = models.SlugField(unique=True, max_length=255, editable=False)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    postPublished = models.DateTimeField(auto_now_add=True)
    postModified = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.postTitle)
        super(Post, self).save(*args, **kwargs)


class PostSave(models.Model):

    profile = models.ForeignKey(
        Profile, related_name="userSavingPost", on_delete=models.CASCADE)
    post = models.ForeignKey(
        Post, related_name="postSaved", on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['profile', 'post'],  name="unique_PostSave")
        ]

        ordering = ["-created"]


Profile.add_to_class('postSave', models.ManyToManyField(

    'self', through=PostSave, related_name='savedPost', symmetrical=False))


class QuestionSave(models.Model):
    profile = models.ForeignKey(
        Profile, related_name="userSavingQuestion", on_delete=models.CASCADE)
    question = models.ForeignKey(
        Question, related_name="QuestionSaved", on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['profile', 'question'],  name="unique_QuestionSave")
        ]

        ordering = ["-created"]


Profile.add_to_class('questionSave', models.ManyToManyField(

    'self', through=QuestionSave, related_name='savedQuestion', symmetrical=False))


class AnswerSave(models.Model):
    profile = models.ForeignKey(
        Profile, related_name="userSavingAnswer", on_delete=models.CASCADE)
    answer = models.ForeignKey(
        Answer, related_name="answerSaved", on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['profile', 'answer'],  name="unique_AnswerSave")
        ]

        ordering = ["-created"]


Profile.add_to_class('answerSave', models.ManyToManyField(

    'self', through=AnswerSave, related_name='savedAnswer', symmetrical=False))
