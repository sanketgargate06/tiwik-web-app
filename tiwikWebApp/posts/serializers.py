from rest_framework import serializers
from .models import Answer, Question, Post, AnswerSave, QuestionSave, PostSave
from django.db import transaction, IntegrityError
from django.http import HttpResponse
from django.utils.text import slugify
from django.shortcuts import get_object_or_404


class AnswerSerializer(serializers.ModelSerializer):
    author = serializers.ReadOnlyField(source='author.full_name')
    author_slug = serializers.ReadOnlyField(source='author.url_slug')
    title = serializers.ReadOnlyField(source='question.title')
    profilePic = serializers.ReadOnlyField(
        source='author.profile.get_image_url')
    slug = serializers.ReadOnlyField(source='question.slug')
    is_saved = serializers.SerializerMethodField()
    savedCount = serializers.SerializerMethodField()
    class Meta:
        model = Answer
        fields = ['id', 'author', 'author_slug', 'content', 'published_on', 'slug',
                  'title', 'profilePic','is_saved','savedCount']

    def get_is_saved(self, instance):
        request = self.context.get('request', None)
        profile = request.user.profile

        saved = AnswerSave.objects.filter(
            answer=instance, profile=request.user.profile)
        if len(saved) == 1:
            return True
        elif len(saved) == 0:
            return False

    def get_savedCount(self, instance):
        saved = AnswerSave.objects.filter(answer=instance)
        return len(saved)

class PostSerializer(serializers.ModelSerializer):
    author = serializers.ReadOnlyField(source='author.full_name')
    author_slug = serializers.ReadOnlyField(source='author.url_slug')
    profilePic = serializers.ReadOnlyField(
        source='author.profile.get_image_url')
    is_saved = serializers.SerializerMethodField()
    savedCount = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = ['id', 'postTitle', 'postContent', 'slug', 'profilePic',
                  'postPublished', 'postModified',    'author',    'author_slug', 'is_saved', 'savedCount']

    def get_is_saved(self, instance):
        request = self.context.get('request', None)
        profile = request.user.profile

        saved = PostSave.objects.filter(
            post=instance, profile=request.user.profile)
        if len(saved) == 1:
            return True
        elif len(saved) == 0:
            return False

    def get_savedCount(self, instance):
        saved = PostSave.objects.filter(post=instance)
        return len(saved)
class QuestionSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(many=True)
    author = serializers.ReadOnlyField(source='author.full_name')
    question_author_slug = serializers.ReadOnlyField(source='author.url_slug')
    slug = serializers.SerializerMethodField(read_only=True)

    def get_slug(self, validated_data):
        return slugify(validated_data.title)

    class Meta:
        model = Question
        fields = ['id', 'url', 'slug', 'author', 'question_author_slug',
                  'title', 'description', 'answers']
        lookup_field = 'slug'
        extra_kwargs = {'url': {'lookup_field': 'slug'}}

    def create(self, validated_data):
        answers_data = validated_data.pop('answers')
        transaction.set_autocommit(False)
        try:
            question = Question.objects.create(**validated_data)
            for answer_data in answers_data:
                answer_data['author'] = self.context['request'].user
                Answer.objects.create(question=question, **answer_data)
        except:
            transaction.rollback()
            raise
        else:
            transaction.commit()
        finally:
            transaction.set_autocommit(True)
        return question

    def update(self, instance, validated_data):
        answers_data = []
        if 'answers' in validated_data:
            answers_data = validated_data.pop('answers')
        answers = (instance.answers).all()
        answers = list(answers)
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get(
            'description', instance.description)
        instance.author = validated_data.get('author', instance.author)
        instance.save()

        if len(answers_data) != 0:
            for answer_data in answers_data:
                if len(answers) == 0:
                    try:
                        question = Question.objects.get(
                            title=validated_data['title'])
                        answer_data['author'] = self.context['request'].user
                        Answer.objects.create(question=question, **answer_data)
                    except:
                        transaction.rollback()
                        raise
                    else:
                        transaction.commit()
                    finally:
                        transaction.set_autocommit(True)
                    return question
                else:
                    answer = answers.pop(0)
                    question = answer.question
                    if answer.author == self.context['request'].user:
                        answer.content = answer_data.get('content', answer.content)
                        answer.save()
                    else:
                        answer_data['author'] = self.context['request'].user
                        Answer.objects.create(question=question, **answer_data)
        return instance


class AnswerSaveSerializer(serializers.ModelSerializer):
    saved_answer = serializers.SerializerMethodField()

    class Meta:
        model = AnswerSave
        fields = ['saved_answer']

    def get_saved_answer(self, obj):
        request = self.context.get("request")
        return AnswerSerializer(obj.answer,context={"request": request}).data


class QuestionSaveSerializer(serializers.ModelSerializer):
    saved_questions = serializers.SerializerMethodField()

    class Meta:
        model = QuestionSave
        fields = ['saved_questions']

    def get_saved_questions(self, obj):
        return QuestionSerializer(obj.question).data


class PostSaveSerializer(serializers.ModelSerializer):
    saved_posts = serializers.SerializerMethodField()

    class Meta:
        model = PostSave
        fields = ['saved_posts']

    def get_saved_posts(self, obj):
        request = self.context.get("request")
        return PostSerializer(obj.post, context={"request": request}).data
