from rest_framework import routers
from .views import PostViewSet, AnswerViewSet, QuestionViewSet, AnswerSaveAPIView, PostSaveAPIView
from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt import views as jwt_views

router = routers.DefaultRouter()
router.register('posts', PostViewSet)
router.register('answers', AnswerViewSet)
router.register('question', QuestionViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('token/obtain/', jwt_views.TokenObtainPairView.as_view(), name='token_create'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('answers/<int:pk>/like',
         AnswerSaveAPIView.as_view(), name='answer-like-api'),
    path('posts/<int:pk>/like', PostSaveAPIView.as_view(), name='post-like-api'),
]
