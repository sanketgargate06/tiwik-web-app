const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');
const path = require('path')

const TARGET = process.env.npm_lifecycle_event;

module.exports = {
  entry: ["babel-polyfill", "./test.js"],

  node: {
    fs: 'empty',
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: TARGET === 'dev' ? '"development"' : '"production"' },
      '__DEVELOPMENT__': TARGET === 'dev'
    }),
    new Dotenv({
      path: './.env'
    })
  ],

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      { test: /\.css$/, loader: "style-loader!css-loader" },
      {
        test: /\.svg$/,
        loader: "svg-inline-loader",
      },
    ],
  },
};
